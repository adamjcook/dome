// MathematicaModel.cpp: implementation of the MathematicaModel class.
//
//////////////////////////////////////////////////////////////////////

#include "MathematicaModel.h"

namespace DOME {
namespace MathematicaPlugin {

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

MathematicaModel::MathematicaModel(string filename) throw(DomeException)
{
	//todo: set isVisible
	//cout<<"mathematica model instantiated!"<< endl;

	_filename = filename;
	_ep = NULL;
	_lp = NULL;
}

MathematicaModel::~MathematicaModel()
{
	unloadModel();
}

bool MathematicaModel::isModelLoaded()
{
	if (_lp != NULL && _ep != NULL) return true;
	return false;
}

void MathematicaModel::loadModel() throw(DomeException)
{
//cout<<"mathematica model loading\n";
	if (isModelLoaded()) return;
	int argc = 4;
	char *argv[5] = {"-linkname", "c:\\Program Files\\Wolfram Research\\Mathematica\\4.1\\MathKernel -mathlink", "-linkmode", "launch", NULL};

	if(!_ep && (_ep = MLInitialize(NULL)) == NULL) {
		_ep = NULL;
		_lp = NULL;
		MATHEMATICA_ERROR("MathematicaModel::loadModel: Error in MLInitialize");
	}

	if(_lp == NULL)
		_lp = MLOpen(argc, argv);
	_createConnections();
}

void MathematicaModel::unloadModel() throw(DomeException)
{
//cout<<"mathematica model unloading\n";
	try{
		_destroyConnections();

		if(_lp != NULL)
		{
			MLPutFunction(_lp, "Exit", 0);
			MLClose(_lp);
		}

		_ep = NULL;
		_lp = NULL; 
	} catch(...) {
		throw;
	}
}

void MathematicaModel::execute() throw(DomeException)
{



	if (_lp == NULL) {
		MATHEMATICA_ERROR("MathematicaModel::execute: MLink pointer is null. Unable to execute model.");
	}
	int pkt;

/*	cout<<"_lp = ";
	cout<<_lp<<endl;
*/
	MLPutFunction(_lp, "EvaluatePacket", 1);
		MLPutFunction(_lp, "Get", 1);
/*		cout<<_filename.c_str();
		cout<<endl;
*/			MLPutString(_lp, _filename.c_str());
	MLEndPacket(_lp);
	MLFlush(_lp);
	// 3 Cases:
		// 1. model loaded correctly and SYMBOL NULL is returned wrapped in a head
		// 2. model loaded correctly and returned the value of the output variable wrapped in a head
		// 3. model didn't load correctly and returned a SYMBOL of MessagePacket

	// have to check for MESSAGEPKTs, because there might be an error in loading the model
	int i = 0;


	while ((pkt = MLNextPacket(_lp)) && pkt != RETURNPKT && pkt != MESSAGEPKT)
	{
		cout<<"in here00";
	    cout<<endl;
		MLNewPacket(_lp);
		i++;
		if (i>100)
			MATHEMATICA_ERROR("MathematicaModel::execute: No return packet. Unable to execute.");
	}

/*	cout<<"pkt = ";
	cout<<pkt<<endl;
*/
	if (!pkt) {
		cout<<"in here1";
	    cout<<endl;
		string message = MLErrorMessage(_lp);
		MLClearError(_lp);
		MATHEMATICA_ERROR("MathematicaModel::execute: "+message);
	}

	// Kernel is sending back warning or error message
	if (pkt == MESSAGEPKT) 
	{
		cout<<"in here now!!!";
	    cout<<endl;
		kcharp_ct name, tag;
		// hopefully a helpful error message
		MLGetSymbol(_lp, &name);
		MLDisownSymbol(_lp,name);

		MLGetString(_lp, &tag);
		MLDisownString(_lp,tag);


		// to break out of loop set lp = NULL
		_lp = NULL;

		string message = "Unable to execute model. Error in ";
		message += _filename;
		message += " "; message += name; message += " "; message += tag;

		CheckPoint;

		MATHEMATICA_ERROR("MathematicaModel::execute: "+message);

	}

//	_readReturnPacket(_lp); // helper function for debugging, prints out the contents of the packet
	MLNewPacket(_lp);


}

MathematicaReal* MathematicaModel::createReal(string name)// throw(DomeException)
{
	MathematicaReal* real = new MathematicaReal(name);
	_data.push_back(real);
	return real;
}

MathematicaMatrix* MathematicaModel::createMatrix(string name, int rows, int columns)// throw(DomeException)
{
	//cout<<"---------------------------"<<endl;
	MathematicaMatrix* matrix = new MathematicaMatrix(name, rows, columns);
	_data.push_back(matrix);

	return matrix;
}

MathematicaInteger* MathematicaModel::createInteger(string name)// throw(DomeException)
{
	MathematicaInteger* integer = new MathematicaInteger(name);
	_data.push_back(integer);
	return integer;
}

MathematicaVector* MathematicaModel::createVector(string name, int rows)// throw(DomeException)
{
	//cout<<"++++++++++++++++++++++++++++"<<endl;
	MathematicaVector* vector = new MathematicaVector(name, rows);
	_data.push_back(vector);

	return vector;
}

void MathematicaModel::_createConnections()// throw(DomeException)
{
	try{
		for (int i=0; i<_data.size(); i++)
		{
			_data[i]->connect(_lp);
		}
	} catch(...) {
		throw;
	}
}

void MathematicaModel::_destroyConnections()// throw(DomeException)
{
	for (int i=0; i<_data.size(); i++)
	{
		_data[i]->disconnect();
	}
}


// helper function for debugging
// prints out the contents of the packet
void MathematicaModel::_readReturnPacket (MLINK lp) 
{	
	kcharp_ct s;
	char buffer[200];
	int n;
	long i, len, j;
	double r;

	switch (MLGetNext(lp)) {
	case MLTKREAL:
		MLGetReal(lp, &r);
		j = sprintf( buffer, "Real: %f", r);
		MATHEMATICA_DEBUG(buffer);
		break;
	case MLTKFUNC:
		MATHEMATICA_DEBUG("function: ");
			
		if (MLGetArgCount(lp, &len) == 0) {
			MATHEMATICA_ERROR(MLErrorMessage(lp));
		} else {
			_readReturnPacket(lp);
			MATHEMATICA_DEBUG("[");
			for (i = 1; i <= len; ++i) {
				_readReturnPacket(lp);
				if (i != len) MATHEMATICA_DEBUG(", ");
			}	
			MATHEMATICA_DEBUG("]");
		}
		break;
	case MLTKSYM:
		MLGetSymbol(lp,&s);
		MATHEMATICA_DEBUG("symbol: " << s);
		MLDisownSymbol(lp,s);
		break;
	case MLTKSTR:
		MLGetString(lp, &s);
		MATHEMATICA_DEBUG("string: " << s);
		MLDisownString(lp,s);
		break;
	case MLTKINT:
		MLGetInteger(lp,&n);
		MATHEMATICA_DEBUG(n);
		break;
	case MLTKERROR:
	default:
		MATHEMATICA_ERROR(MLErrorMessage(lp));
	}
	return;
} // end _readReturnPacket()

} // namespace MathematicaPlugin
} // DOME

#ifndef DOME_UGINCLUDES_H
#define DOME_UGINCLUDES_H

#if _MSC_VER > 1000
#pragma once
#endif

#if defined(_MSC_VER)
#pragma warning (disable: 4786) 
#endif

#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <uf.h>

#include <uf_assem.h>
#include <uf_cfi.h>
#include <uf_drf.h>
#include <uf_exit.h>
#include <uf_obj.h>
#include <uf_object_types.h>
#include <uf_layout.h>
#include <uf_modl.h>
#include <uf_std.h>
#include <uf_view.h>
#include <uf_web.h>
#include <uf_ui.h>


#include <ug_assembly_node.hxx>
#include <ug_body.hxx>
#include <ug_displayable.hxx>
#include <ug_exception.hxx>
#include <ug_expression.hxx>
#include <ug_info_window.hxx>
#include <ug_session.hxx>
#include <ug_iterator.hxx>
#include <ug_object.hxx>
#include <ug_part.hxx>
//#include <ug_string.hxx>
#include <ug_typed.hxx>
//#include <ug_vector.hxx>

#include <string>
using std::string;
#include <vector>
using std::vector;
#include <iostream>   // std::cout, std::endl
using std::cout;   
using std::endl;   

#define NUM_OUTPUT_TYPES 14
#define MAX_LN_SIZE 300 // if this value is not big enough, program will crash esp. with translators

#define UG_SWITCH true
#define UG_DEBUG(s) if (UG_SWITCH) cout << "Unigraphics Debug: " << s << endl;
#define UG_PRINT(s) cout << "Unigraphics Print: " << s << endl;
#define UG_ERROR(s) cout << "Unigraphics Error: " << s << endl;

#define UF_CALL(X) (report_error( __FILE__, __LINE__, #X, (X)))
static int report_error( char *file, int line, char *call, int irc)
{
    if (irc)
    {
        char err[133],
             msg[133];

        sprintf(msg, "*** ERROR code %d at line %d in %s:\n+++ ",
            irc, line, file);
        UF_get_fail_message(irc, err);

    /*  NOTE:  UF_print_syslog is new in V18 */

        UF_print_syslog(msg, FALSE);
        UF_print_syslog(err, FALSE);
        UF_print_syslog("\n", FALSE);
        UF_print_syslog(call, FALSE);
        UF_print_syslog(";\n", FALSE);

        if (!UF_UI_open_listing_window())
        {
            UF_UI_write_listing_window(msg);
            UF_UI_write_listing_window(err);
            UF_UI_write_listing_window("\n");
            UF_UI_write_listing_window(call);
            UF_UI_write_listing_window(";\n");
        }
    }

    return(irc);
}


#endif // DOME_UGINCLUDES_H
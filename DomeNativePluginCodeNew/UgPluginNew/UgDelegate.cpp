//----------------------------------------------------------------------------
// UgDelegate.cpp
//
// Purpose: unwraps command, data objects and data values supplied by
// the caller. Instantiates model and data objects. Delegates work
// to the model and data objects.
//
// *********** PLUGIN AUTHOR MODIFIES EVERYTHING BELOW THIS LINE ************
// ***********					  AS NEEDED                      ************
//----------------------------------------------------------------------------

#ifdef _WINDOWS
#pragma warning(disable:4786)	// stl warnings
#endif
#include <vector>
#include <string>
#include <iostream>
using namespace std;

// java native interface stuff
#ifdef _WINDOWS
#include <jni.h>
#else
#include "../jni_linux/jni.h"
#endif
#include "JNIHelper.h"

// Ug function prototypes
#include "UgIncludes.h"

// data and model classes
#include "UgData.h"
#include "UgModel.h"
using namespace DOME::UgPlugin;

// definitions for this class
#include "UgPluginCaller.h"
#include "UgDelegate.h"



/////////////////////////////////////////////////////////////////////
// invoke all functions that return an object (casted to a long)
/////////////////////////////////////////////////////////////////////
long UgDelegate::callObjectFunctions (JNIEnv* env, jobjectArray args, 
										 unsigned int iNumArgs,
										 long iObjectPtr,
										 unsigned int iFunctionIndex)
{
	// call the appropriate function
	switch (iFunctionIndex)
	{
	case UgPluginCaller_MODEL_INIT:
		iObjectPtr = (long) initializeModel (env, args, iNumArgs);
		break;

	case UgPluginCaller_MODEL_CREATE_COMPONENT:
		iObjectPtr = (long) createComponent (env, args, iNumArgs, iObjectPtr);
		break;

	case UgPluginCaller_MODEL_CREATE_VRMLFILE:
		iObjectPtr = (long) createVrmlFile (env, args, iNumArgs, iObjectPtr);
		break;

	case UgPluginCaller_MODEL_CREATE_IMPORTFILE:
		iObjectPtr = (long) createImportFile (env, args, iNumArgs, iObjectPtr);
		break;

	case UgPluginCaller_COMPONENT_CREATE_DIMENSIONIN:
		iObjectPtr = (long) createDimensionIn (env, args, iNumArgs, iObjectPtr);
		break;

	case UgPluginCaller_COMPONENT_CREATE_DIMENSIONOUT:
		iObjectPtr = (long) createDimensionOut (env, args, iNumArgs, iObjectPtr);
		break;

	case UgPluginCaller_COMPONENT_CREATE_MASSPROPERTY:
		iObjectPtr = (long) createMassProperty (env, args, iNumArgs, iObjectPtr);
		break;

	case UgPluginCaller_COMPONENT_CREATE_EXPORTFILE:
		iObjectPtr = (long) createExportFile (env, args, iNumArgs, iObjectPtr);
		break;

	default:
		UG_DEBUG ("callObjectFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return iObjectPtr;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return void
/////////////////////////////////////////////////////////////////////
void UgDelegate::callVoidFunctions (JNIEnv* env, jobjectArray args, 
									   unsigned int iNumArgs,
									   long iObjectPtr,
									   unsigned int iFunctionIndex)
{
	// call the appropriate function
	switch (iFunctionIndex)
	{
	case UgPluginCaller_MODEL_LOAD:
		loadModel (iObjectPtr);
		break;

	case UgPluginCaller_MODEL_EXECUTE:
		executeModel (iObjectPtr);
		break;

	case UgPluginCaller_MODEL_EXEC_BF_INPUT:
		break;

	case UgPluginCaller_MODEL_EXEC_AF_INPUT:
		break;

	case UgPluginCaller_MODEL_UNLOAD:
		unloadModel (iObjectPtr);
		break;

	case UgPluginCaller_MODEL_DESTROY:
		destroyModel (iObjectPtr);
		break;

	case UgPluginCaller_MODEL_IMPORT:
		import (iObjectPtr);
		break;

	case UgPluginCaller_MODEL_EXPORT:
		export (iObjectPtr);
		break;

	case UgPluginCaller_IMPORTFILE_SET_CHANGED:
		importfileSetChanged (env, args, iNumArgs, iObjectPtr);
		break;

	case UgPluginCaller_DIMENSIONIN_SET_VALUE:
		setDimensionInValue (env, args, iNumArgs, iObjectPtr);
		break;

	default:
		UG_DEBUG ("callVoidFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return an integer
/////////////////////////////////////////////////////////////////////
int UgDelegate::callIntegerFunctions (JNIEnv* env, jobjectArray args, 
										 unsigned int iNumArgs,
										 long iObjectPtr,
										 unsigned int iFunctionIndex)
{
	int value = 0;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	default:
		UG_DEBUG ("callIntegerFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return value;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a boolean
/////////////////////////////////////////////////////////////////////
bool UgDelegate::callBooleanFunctions (JNIEnv* env, jobjectArray args, 
										  unsigned int iNumArgs,
										  long iObjectPtr,
										  unsigned int iFunctionIndex)
{
	bool value = false;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	case UgPluginCaller_MODEL_IS_LOADED:
		value = isModelLoaded (iObjectPtr);
		break;

	case UgPluginCaller_IMPORTFILE_GET_CHANGED:
		value = importfileGetChanged (iObjectPtr);
		break;

	default:
		UG_DEBUG ("callBooleanFunctions: Undefined function index ("
					<< iFunctionIndex << ")" << endl);
	}

	return value;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a double
/////////////////////////////////////////////////////////////////////
double UgDelegate::callDoubleFunctions (JNIEnv* env, jobjectArray args, 
										   unsigned int iNumArgs,
										   long iObjectPtr,
										   unsigned int iFunctionIndex)
{
	//for debug
	//cout << "Get into UgDelegate::callDoubleFunctions!" << endl;
	//cout << "iFunctionIndex = " << iFunctionIndex << ", UgPluginCaller_REAL_GET_VALUE = "<< UgPluginCaller_REAL_GET_VALUE << endl;
	
	double value = 0;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	case UgPluginCaller_DIMENSIONIN_GET_VALUE:
		value = getDimensionInValue (iObjectPtr);
		break;

	case UgPluginCaller_MASSPROPERTY_GET_VALUE:
		value = getMasspropertyValue (iObjectPtr);
		break;

	default:
		UG_DEBUG ("callDoubleFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return value;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a character string
/////////////////////////////////////////////////////////////////////
const 
char* UgDelegate::callStringFunctions (JNIEnv* env, jobjectArray args, 
										  unsigned int iNumArgs,
										  long iObjectPtr,
										  unsigned int iFunctionIndex)
{
	const char * value = NULL;

	// call the appropriate function
	switch (iFunctionIndex)
	{

	default:
		UG_DEBUG ("callStringFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return value;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a 1D array of doubles
/////////////////////////////////////////////////////////////////////
vector<double>
UgDelegate::callDoubleArrayFunctions (JNIEnv* env, jobjectArray args,
										 unsigned int iNumArgs,
										 long iObjectPtr,
										 unsigned int iFunctionIndex)
{
	vector<double> result;

	// call the appropriate function
	switch (iFunctionIndex)
	{

	default:
		UG_DEBUG ("callDoubleArrayFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return result;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a 1D array of integers
/////////////////////////////////////////////////////////////////////
vector<int>
UgDelegate::callIntegerArrayFunctions (JNIEnv* env, jobjectArray args,
										  unsigned int iNumArgs,
										  long iObjectPtr,
										  unsigned int iFunctionIndex)
{
	vector<int> result;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	default:
		UG_DEBUG ("callIntegerArrayFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return result;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a 2D array of integers
/////////////////////////////////////////////////////////////////////
vector< vector<int> >
UgDelegate::call2DIntegerArrayFunctions (JNIEnv* env, jobjectArray args,
											unsigned int iNumArgs,
											long iObjectPtr,
											unsigned int iFunctionIndex)
{
	vector < vector<int> > result;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	default:
		UG_DEBUG ("call2DIntegerArrayFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return result;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a 2D array of doubles
/////////////////////////////////////////////////////////////////////
vector< vector<double> > 
UgDelegate::call2DDoubleArrayFunctions (JNIEnv* env, jobjectArray args,
										   unsigned int iNumArgs,
										   long iObjectPtr,
										   unsigned int iFunctionIndex)
{
	vector< vector<double> > result;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	default:
		UG_DEBUG ("call2DDoubleArrayFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return result;
}



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// void functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


/////////////////////////////////////////////////////
// load the model
/////////////////////////////////////////////////////
void UgDelegate::loadModel (long iObjectPtr)
{
	UgModel *model = (UgModel*) iObjectPtr;

	if (model == NULL) {
		UG_DEBUG ("loadModel: Model does not exist" << endl);
	}
	else
	{
		model->loadModel ();
	}
}


/////////////////////////////////////////////////////
// unload the model
/////////////////////////////////////////////////////
void UgDelegate::unloadModel (long iObjectPtr)
{
	UgModel *model = (UgModel*) iObjectPtr;

	if (model == NULL) {
		UG_DEBUG ("unloadModel: Model does not exist" << endl);
	}
	else
	{
		model->unloadModel ();
	}
}


/////////////////////////////////////////////////////
// destroy the model
/////////////////////////////////////////////////////
void UgDelegate::destroyModel (long iObjectPtr)
{
	UgModel *model = (UgModel*) iObjectPtr;

	if (model == NULL) {
		UG_DEBUG ("destroyModel: Model does not exist" << endl);
	}
	else
	{
		delete model;
	}
}

/////////////////////////////////////////////////////
// import
/////////////////////////////////////////////////////
void UgDelegate::import (long iObjectPtr)
{
	UgModel *model = (UgModel*) iObjectPtr;

	if (model == NULL) {
		UG_DEBUG ("unloadModel: Model does not exist" << endl);
	}
	else
	{
		model->import ();
	}
}

/////////////////////////////////////////////////////
// export
/////////////////////////////////////////////////////
void UgDelegate::export (long iObjectPtr)
{
	UgModel *model = (UgModel*) iObjectPtr;

	if (model == NULL) {
		UG_DEBUG ("unloadModel: Model does not exist" << endl);
	}
	else
	{
		model->export ();
	}
}

/////////////////////////////////////////////////////
// importfile set changed
/////////////////////////////////////////////////////
void UgDelegate::importfileSetChanged (JNIEnv* env, jobjectArray args,
								   unsigned int iNumArgs, long iObjectPtr)
{

	// check argument list length
	if (iNumArgs != 1) {
		UG_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the importfileSetChanged function");
	}
	else 
	{
		UgImportFile *iptFile = (UgImportFile*) iObjectPtr;

		if (iptFile == NULL) {
			UG_DEBUG ("importfileSetChanged: importfile does not exist" << endl);
		}
		else
		{
			int err;
			void *value = NULL;

			// unpack the argument and call the function
			err = getArgument (env, args, 0, JAVA_BOOLEAN, value);
			if (err != ERR_OK) {
				UG_DEBUG ("importfileSetChanged: Invalid parameter value (ERR = "
							 << err << ")" << endl);
			}
			else {
				iptFile->setChanged (*((bool*)value));

			}

			// clean up
			if (value != NULL) delete (bool*) value;
		}
	}

}


/////////////////////////////////////////////////////
// set dimensionin values
/////////////////////////////////////////////////////
void UgDelegate::setDimensionInValue (JNIEnv* env, jobjectArray args,
								   unsigned int iNumArgs,
								   long iObjectPtr)
{
	// check argument list length
	if (iNumArgs != 1) {
		UG_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the setDimensionInValue function");
	}
	else 
	{
		//UgReal *parameter = (UgReal*) iObjectPtr;
		UgDimensionIn *parameter = (UgDimensionIn*) iObjectPtr;
		if (parameter == NULL) {
			UG_DEBUG ("setDimensionInValue: Parameter does not exist" << endl);
		}
		else
		{
			int err;
			void *value = NULL;

			// unpack the argument and call the function
			err = getArgument (env, args, 0, JAVA_DOUBLE, value);
			if (err != ERR_OK) {
				UG_DEBUG ("setDimensionInValue: Invalid parameter value (ERR = "
							 << err << ")" << endl);
			}
			else {
				//cout << "setRealValue: " << *((double*)value) << endl;
				parameter->setValue ( *((double*)value) );
			}

			// clean up
			if (value != NULL) delete (double*) value;
		}
	}
}



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// object functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


///////////////////////////////////////////
// create the model
///////////////////////////////////////////
UgModel *UgDelegate::initializeModel (JNIEnv* env, jobjectArray args,
											  unsigned int iNumArgs)
{
	UgModel *model = NULL;

	// check argument list length
	if (iNumArgs != 2) {
		UG_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the initializeModel function" << endl);
	}
	else 
	{
		int err1, err2;
		void* szDirName = NULL;
		void* szModName = NULL;

		// unpack the arguments
		err1 = getArgument (env, args, 0, JAVA_STRING, szDirName);
		err2 = getArgument (env, args, 1, JAVA_STRING, szModName);

		if (err1 != ERR_OK) {
			UG_DEBUG ("initializeModel: Invalid model file directory name (ERR = "
						 << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			UG_DEBUG ("initializeModel: Invalid model file name (ERR = " 
						 << err2 << ")" << endl);
		}
		else {
			// call the constructor
			model = new UgModel (  (char*)szDirName, 
									  (char*)szModName);
		}

		// clean up
		if (szDirName != NULL) delete (char*) szDirName;
		if (szModName != NULL) delete (char*) szModName;
	}

	return model;
}

///////////////////////////////////////////
// create component objects
///////////////////////////////////////////
UgComponent *UgDelegate::createComponent (JNIEnv* env, jobjectArray args,
											   unsigned int iNumArgs,
											   long iObjectPtr)
{
	UgComponent *modelObject = NULL;

	// check argument list length
	if (iNumArgs != 2) {
		UG_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the createComponent function" << endl);
	}
	else 
	{
		int err1,err2;
		void* szParameterName = NULL;
		void* szUnitName = NULL;

		// unpack the arguments
		UgModel *model = (UgModel*) iObjectPtr;
		err1 = getArgument (env, args, 0, JAVA_STRING, szParameterName);
		err2 = getArgument (env, args, 1, JAVA_STRING, szUnitName);

		if (model == NULL) {
			UG_DEBUG ("createComponent: Model does not exist" << endl);
		}
		else
		if (err1 != ERR_OK) {
			UG_DEBUG ("createComponent: Invalid parameter name (ERR = "
						 << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			UG_DEBUG ("createComponent: Invalid unit name (ERR = "
						 << err2 << ")" << endl);
		}
		else
		{
			// call the function
			string name ((char*)szParameterName);
			string unitname ((char*)szUnitName);
			modelObject = model->createComponent (name, unitname);
		}

		// clean up
		if (szParameterName != NULL) delete (char*) szParameterName;
		if (szUnitName != NULL) delete (char*) szUnitName;
	}

	return modelObject;
}

///////////////////////////////////////////
// create vrml file objects
///////////////////////////////////////////
UgVrmlFile *UgDelegate::createVrmlFile (JNIEnv* env, jobjectArray args,
											   unsigned int iNumArgs,
											   long iObjectPtr)
{
	UgVrmlFile *modelObject = NULL;

	// check argument list length
	if (iNumArgs != 1) {
		UG_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the createVrmlFile function" << endl);
	}
	else 
	{
		int err;
		void* szParameterName = NULL;

		// unpack the arguments
		UgModel *model = (UgModel*) iObjectPtr;
		err = getArgument (env, args, 0, JAVA_STRING, szParameterName);

		if (model == NULL) {
			UG_DEBUG ("createVrmlFile: Model does not exist" << endl);
		}
		else
		if (err != ERR_OK) {
			UG_DEBUG ("createVrmlFile: Invalid parameter name (ERR = "
						 << err << ")" << endl);
		}
		else
		{
			// call the function
			string name ((char*)szParameterName);
			modelObject = model->createVrmlFile (name);
		}

		// clean up
		if (szParameterName != NULL) delete (char*) szParameterName;
	}

	return modelObject;
}

///////////////////////////////////////////
// create import file objects
///////////////////////////////////////////
UgImportFile *UgDelegate::createImportFile (JNIEnv* env, jobjectArray args,
											   unsigned int iNumArgs,
											   long iObjectPtr)
{
	UgImportFile *modelObject = NULL;

	// check argument list length
	if (iNumArgs != 1) {
		UG_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the createImportFile function" << endl);
	}
	else 
	{
		int err;
		void* szParameterName = NULL;

		// unpack the arguments
		UgModel *model = (UgModel*) iObjectPtr;
		err = getArgument (env, args, 0, JAVA_STRING, szParameterName);

		if (model == NULL) {
			UG_DEBUG ("createImportFile: Model does not exist" << endl);
		}
		else
		if (err != ERR_OK) {
			UG_DEBUG ("createImportFile: Invalid parameter name (ERR = "
						 << err << ")" << endl);
		}
		else
		{
			// call the function
			string name ((char*)szParameterName);
			modelObject = model->createImportFile (name);
		}

		// clean up
		if (szParameterName != NULL) delete (char*) szParameterName;
	}

	return modelObject;
}

///////////////////////////////////////////////////////////////////
// create dimensionIn objects by UgComponent instead of UgModel
///////////////////////////////////////////////////////////////////
UgDimensionIn *UgDelegate::createDimensionIn (JNIEnv* env, jobjectArray args,
											   unsigned int iNumArgs,
											   long iObjectPtr)
{
	UgDimensionIn *componentObject = NULL;

	// check argument list length
	if (iNumArgs != 1) {
		UG_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the createDimensionIn function" << endl);
	}
	else 
	{
		int err;
		void* szParameterName = NULL;

		// unpack the arguments
		UgComponent *component = (UgComponent*) iObjectPtr;
		err = getArgument (env, args, 0, JAVA_STRING, szParameterName);

		if (component == NULL) {
			UG_DEBUG ("createDimensionIn: Component does not exist" << endl);
		}
		else
		if (err != ERR_OK) {
			UG_DEBUG ("createDimensionIn: Invalid parameter name (ERR = "
						 << err << ")" << endl);
		}
		else
		{
			// call the function
			string name ((char*)szParameterName);
			componentObject = component->createDimensionIn (name);
		}

		// clean up
		if (szParameterName != NULL) delete (char*) szParameterName;
	}

	return componentObject;
}

///////////////////////////////////////////////////////////////////
// create dimensionOut objects by UgComponent instead of UgModel
///////////////////////////////////////////////////////////////////
UgDimensionOut *UgDelegate::createDimensionOut (JNIEnv* env, jobjectArray args,
											   unsigned int iNumArgs,
											   long iObjectPtr)
{
	UgDimensionOut *componentObject = NULL;

	// check argument list length
	if (iNumArgs != 1) {
		UG_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the createDimensionOut function" << endl);
	}
	else 
	{
		int err;
		void* szParameterName = NULL;

		// unpack the arguments
		UgComponent *component = (UgComponent*) iObjectPtr;
		err = getArgument (env, args, 0, JAVA_STRING, szParameterName);

		if (component == NULL) {
			UG_DEBUG ("createDimensionOut: Component does not exist" << endl);
		}
		else
		if (err != ERR_OK) {
			UG_DEBUG ("createDimensionOut: Invalid parameter name (ERR = "
						 << err << ")" << endl);
		}
		else
		{
			// call the function
			string name ((char*)szParameterName);
			componentObject = component->createDimensionOut (name);
		}

		// clean up
		if (szParameterName != NULL) delete (char*) szParameterName;
	}

	return componentObject;
}

///////////////////////////////////////////////////////////////////
// create UgMassProperty objects by UgComponent instead of UgModel
///////////////////////////////////////////////////////////////////
UgMassProperty *UgDelegate::createMassProperty (JNIEnv* env, jobjectArray args,
											   unsigned int iNumArgs,
											   long iObjectPtr)
{
	UgMassProperty *componentObject = NULL;

	// check argument list length
	if (iNumArgs != 1) {
		UG_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the createMassProperty function" << endl);
	}
	else 
	{
		int err;
		void* szParameterName = NULL;

		// unpack the arguments
		UgComponent *component = (UgComponent*) iObjectPtr;
		err = getArgument (env, args, 0, JAVA_STRING, szParameterName);

		if (component == NULL) {
			UG_DEBUG ("createMassProperty: Component does not exist" << endl);
		}
		else
		if (err != ERR_OK) {
			UG_DEBUG ("createMassProperty: Invalid parameter name (ERR = "
						 << err << ")" << endl);
		}
		else
		{
			// call the function
			string name ((char*)szParameterName);
			componentObject = component->createMassProperty (name);
		}

		// clean up
		if (szParameterName != NULL) delete (char*) szParameterName;
	}

	return componentObject;
}

///////////////////////////////////////////////////////////////////
// create UgMassProperty objects by UgComponent instead of UgModel
///////////////////////////////////////////////////////////////////
UgExportFile *UgDelegate::createExportFile (JNIEnv* env, jobjectArray args,
											   unsigned int iNumArgs,
											   long iObjectPtr)
{
	UgExportFile *componentObject = NULL;

	// check argument list length
	if (iNumArgs != 1) {
		UG_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the createExportFile function" << endl);
	}
	else 
	{
		int err;
		void* szParameterName = NULL;

		// unpack the arguments
		UgComponent *component = (UgComponent*) iObjectPtr;
		err = getArgument (env, args, 0, JAVA_STRING, szParameterName);

		if (component == NULL) {
			UG_DEBUG ("createExportFile: Component does not exist" << endl);
		}
		else
		if (err != ERR_OK) {
			UG_DEBUG ("createExportFile: Invalid parameter name (ERR = "
						 << err << ")" << endl);
		}
		else
		{
			// call the function
			string name ((char*)szParameterName);
			componentObject = component->createExportFile (name);
		}

		// clean up
		if (szParameterName != NULL) delete (char*) szParameterName;
	}

	return componentObject;
}



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// real functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


///////////////////////////////////////////
// get the value from dimensionIn
///////////////////////////////////////////
double UgDelegate::getDimensionInValue (long iObjectPtr)
{
	//for debug
	//cout << "Get into UgDelegate::getDoubleValue" << endl;

	double value = 0;
	//UgReal *data = (UgReal*) iObjectPtr;
	UgDimensionIn *data = (UgDimensionIn*) iObjectPtr;

	if (data == NULL) {
		UG_DEBUG ("getDimensionInValue: UgDimensionIn does not exist" << endl);
	}
	else {
		value = data->getValue ();
	}

	return value;
}

///////////////////////////////////////////
// get the value from Massproperty
///////////////////////////////////////////
double UgDelegate::getMasspropertyValue (long iObjectPtr)
{
	//for debug
	//cout << "Get into UgDelegate::getDoubleValue" << endl;

	double value = 0;
	//UgReal *data = (UgReal*) iObjectPtr;
	UgMassProperty *data = (UgMassProperty*) iObjectPtr;

	if (data == NULL) {
		UG_DEBUG ("getMasspropertyValue: UgMassProperty does not exist" << endl);
	}
	else {
		value = data->getValue ();
	}

	return value;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// integer functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


///////////////////////////////////////////
// get an integer value
///////////////////////////////////////////
int UgDelegate::getIntegerValue (long iObjectPtr,
									 unsigned int iFunctionIndex)
{
	int value = 0;

	return value;
}


///////////////////////////////////////////
// execute the model
///////////////////////////////////////////
void UgDelegate::executeModel (long iObjectPtr)
{
	UgModel *model = (UgModel*) iObjectPtr;

	if (model == NULL) {
		UG_DEBUG ("executeModel: UgModel does not exist" << endl);
	}
	else {
		// execute the model
		model->execute ();
	}
}



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// boolean functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


///////////////////////////////////////////
// get the value from a real parameter
///////////////////////////////////////////
bool UgDelegate::getBooleanValue (long iObjectPtr)
{
	bool value = false;
	return value;
}

///////////////////////////////////////////
// check if model is loaded
///////////////////////////////////////////
bool UgDelegate::isModelLoaded (long iObjectPtr)
{
	bool value = false;
	UgModel *model = (UgModel*) iObjectPtr;

	if (model == NULL) {
		UG_DEBUG ("isModelLoaded: Model does not exist" << endl);
	}
	else {
		// get the value
		value = model->isModelLoaded ();
	}

	return value;
}

///////////////////////////////////////////
// check if importfile is changed
///////////////////////////////////////////
bool UgDelegate::importfileGetChanged (long iObjectPtr)
{
	bool value = false;
	UgImportFile *iptFile = (UgImportFile*) iObjectPtr;

	if (iptFile == NULL) {
		UG_DEBUG ("importfileGetChanged: importfile does not exist" << endl);
	}
	else {
		// get the value
		value = iptFile->getChanged ();
	}

	return value;
}


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// string functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------




//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// 1D array functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


/////////////////////////////////////////////////
// get the value from a double array parameter
/////////////////////////////////////////////////


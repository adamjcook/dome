// UgModel.h: interface for the UgModel class.
//
//////////////////////////////////////////////////////////////////////

#ifndef DOME_UGMODEL_H
#define DOME_UGMODEL_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "UgData.h"

namespace DOME {
namespace UgPlugin {

class UgBoolean;
class UgComponent;
class UgImportFile;
	
class UgModel 
{
public:
    // constructor method should take any parameters
    // necessary to specify model parameters
    UgModel(string directory, string mod);

    // destructor method should clean up all resources
    // used by the model
    virtual ~UgModel();

    // plugins support various IDEAS datatypes
    // "factory" methods should be created for each type supported
    // method should create the derived datatype, store a reference to it,
    // and return a pointer to it
    // virtual IdeasData* createData() /*throw(DomeException)*/;
    // for example:
    UgComponent* createComponent(string componentName) /*throw(DomeException)*/;
    UgComponent* createComponent(string componentName, string unitName) /*throw(DomeException)*/;
    UgImportFile* createImportFile(string componentName);
	UgVrmlFile* createVrmlFile(string componentName);

    // starts the application, loads the necessary files,
    // connects all the data to the underlying source
    void loadModel() /*throw(DomeException)*/;

    // disconnects data from underlying source,
    // quits the files, closes application as necessary
    void unloadModel() /*throw(DomeException)*/;

    // these are the hooks into the execution process
    // a typical model execution will follow the following sequence:
    // 1. executeBeforeInput
    // 2. set values of inputs
    // 3. execute
    // 4. get values of outputs
    // 5. executeAfterOutput
    void execute() /*throw(DomeException)*/; // update model with new input values

    // returns if the model is loaded or not
    // a model is loaded if the third party application is
    // running and all the parameters in the model are
    // "connected" to the application
    bool isModelLoaded();

	int validatePart();
	void startUgDOME();
	void createTempDirectory();
	void getAllComponentParts();
	string extract_part_name( string );
	void storeDOMEunit();
	void getPartsInfo();
	void extractMatingObjects();
	void autoImport(int);
	void importPart(int);
	void findImportChildren();
	void assignMatingObjects(int, UgPart*);
	void setExportBool(bool in) { exportNow.setValue(in); }
	void startVRML();
	void backupImport();
	void createDensityFile();
	void parseDensityFile();
	double matchDensity(int, string);
	double matchDensity2(int);

	
	void export();
	void import();
	void cleanAll();

	void setVrmlIndex(int in) { vrmlIndexOld = vrmlIndex; vrmlIndex = in; }
	void setTypeIndicator(int in) { typeIndicator = in; }
	void findParents();
protected:	  
    string dirName;
    string model;
	string fullModelName;
	bool _isModelLoaded;
	string import_directory;
	string export_directory;
	string UGII_BASE_DIR;
	vector <UgComponent*> components;
    vector <UgImportFile*> imports;
	UgVrmlFile* vrmlFile;
	int storedImport;
	UgBoolean exportNow;
	bool loadedAlready;
	vector <string> baseNames; // for density application
	vector <double> baseDensities; // for density application
	vector <string> MassName; // stores all mass names
	int progFail;

	int vrmlIndex; // consider removing these items to the vrml class for easier linking to vrml module
	int vrmlIndexOld;
	int typeIndicator; // -1 for assembly, -2 for part; irrelevant, don't need it at all, but have to declare for consistency with java
	vector <int> parentIndex;
	vector <string> allCompExists;
};

} // namespace UgPlugin
} // namespace DOME

#endif // DOME_UGPLUGIN_H
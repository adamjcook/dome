//----------------------------------------------------------------------------
// ExcelDelegate.h
//
// *********** PLUGIN AUTHOR MODIFIES EVERYTHING BELOW THIS LINE ************
// ***********          (EXCEPT WHERE NOTED) AS NEEDED           ************
//----------------------------------------------------------------------------


#ifndef DOME_ExcelDelegate_H
#define DOME_ExcelDelegate_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "ExcelModel.h"
using namespace DOME::ExcelPlugin;


class ExcelDelegate
{
public:
	ExcelDelegate () {}


	//////////////////////////////////////////////////////////////////////////////////////
	// entry points to this class
	//////////////////////////////////////////////////////////////////////////////////////
	//
	// *********** PLUGIN AUTHOR DOES NOT MODIFY THIS SECTION (begin) ************

	void     callVoidFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								long iObjectPtr, unsigned int iFunctionIndex);

	int      callIntegerFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								   long iObjectPtr, unsigned int iFunctionIndex);

	double	 callDoubleFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								  long iObjectPtr, unsigned int iFunctionIndex);

	bool     callBooleanFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								   long iObjectPtr, unsigned int iFunctionIndex);

	const
	char*    callStringFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								  long iObjectPtr, unsigned int iFunctionIndex);

	long     callObjectFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								  long iObjectPtr, unsigned int iFunctionIndex);

	vector<int>
		callIntegerArrayFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								   long iObjectPtr, unsigned int iFunctionIndex);

	vector<double>
		callDoubleArrayFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								  long iObjectPtr, unsigned int iFunctionIndex);

	vector< vector<int> >
		call2DIntegerArrayFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
									 long iObjectPtr, unsigned int iFunctionIndex);

	vector< vector<double> > 
		call2DDoubleArrayFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
									long iObjectPtr, unsigned int iFunctionIndex);

	// *********** PLUGIN AUTHOR DOES NOT MODIFY THIS SECTION (end) ************
	//////////////////////////////////////////////////////////////////////////////////////





	//////////////////////////////////////////////////////////////////////////////////////
	// object functions
	//////////////////////////////////////////////////////////////////////////////////////

	ExcelModel  *initializeModel (JNIEnv* env, jobjectArray args, unsigned int iNumArgs);

	ExcelData   *createModelObject (JNIEnv* env, jobjectArray args,
									 unsigned int iNumArgs, long iObjectPtr);

	ExcelData   *createStringObject (JNIEnv* env, jobjectArray args,
									 unsigned int iNumArgs, long iObjectPtr);

	ExcelData   *createMatrix (JNIEnv* env, jobjectArray args, 
								unsigned int iNumArgs, long iObjectPtr);


	//////////////////////////////////////////////////////////////////////////////////////
	// void functions
	//////////////////////////////////////////////////////////////////////////////////////

	void	executeModel (long iObjectPtr);
	void	loadModel (long iObjectPtr);
	void	unloadModel (long iObjectPtr);
	void	destroyModel (long iObjectPtr);
	void	setRealValue (JNIEnv* env, jobjectArray args,
						  unsigned int iNumArgs, long iObjectPtr);
	void	setStringValue (JNIEnv* env, jobjectArray args,
						  unsigned int iNumArgs, long iObjectPtr);
	void	setMatrixElement (JNIEnv* env, jobjectArray args,
							  unsigned int iNumArgs, long iObjectPtr);
	void	setMatrixValues (JNIEnv* env, jobjectArray args,
							 unsigned int iNumArgs, long iObjectPtr);
	void	setMatrixDimension (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								long iObjectPtr, unsigned int iFunctionIndex);
	void	setMatrixDimensions (JNIEnv* env, jobjectArray args, 
								 unsigned int iNumArgs, long iObjectPtr);


	//////////////////////////////////////////////////////////////////////////////////////
	// real functions
	//////////////////////////////////////////////////////////////////////////////////////

	double	getDoubleValue (long iObjectPtr);
	double	getMatrixElement (JNIEnv* env, jobjectArray args,
							  unsigned int iNumArgs, long iObjectPtr);

	//////////////////////////////////////////////////////////////////////////////////////
	// integer functions
	//////////////////////////////////////////////////////////////////////////////////////

	int		getIntegerValue (long iObjectPtr, unsigned int iFunctionIndex);


	//////////////////////////////////////////////////////////////////////////////////////
	// boolean functions
	//////////////////////////////////////////////////////////////////////////////////////

	bool	getBooleanValue (long iObjectPtr);
	bool	isModelLoaded (long iObjectPtr);

	
	//////////////////////////////////////////////////////////////////////////////////////
	// string functions
	//////////////////////////////////////////////////////////////////////////////////////

	//const
	char*	getStringValue (long iObjectPtr);

	
	//////////////////////////////////////////////////////////////////////////////////////
	// 1D array functions
	//////////////////////////////////////////////////////////////////////////////////////

	vector<double> 
			getDoubleArrayValue (JNIEnv* env, jobjectArray args,
								 unsigned int iNumArgs, long iObjectPtr);

	//////////////////////////////////////////////////////////////////////////////////////
	// 2D array functions
	//////////////////////////////////////////////////////////////////////////////////////
	vector< vector<double> > 
			getMatrixElements (long iObjectPtr);

};


#endif // DOME_ExcelDelegate_H

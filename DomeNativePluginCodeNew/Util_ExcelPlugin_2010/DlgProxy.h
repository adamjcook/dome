
// DlgProxy.h: header file
//

#pragma once

class CUtil_ExcelPlugin_2010Dlg;


// CUtil_ExcelPlugin_2010DlgAutoProxy command target

class CUtil_ExcelPlugin_2010DlgAutoProxy : public CCmdTarget
{
	DECLARE_DYNCREATE(CUtil_ExcelPlugin_2010DlgAutoProxy)

	CUtil_ExcelPlugin_2010DlgAutoProxy();           // protected constructor used by dynamic creation

// Attributes
public:
	CUtil_ExcelPlugin_2010Dlg* m_pDialog;

// Operations
public:

// Overrides
	public:
	virtual void OnFinalRelease();

// Implementation
protected:
	virtual ~CUtil_ExcelPlugin_2010DlgAutoProxy();

	// Generated message map functions

	DECLARE_MESSAGE_MAP()
	DECLARE_OLECREATE(CUtil_ExcelPlugin_2010DlgAutoProxy)

	// Generated OLE dispatch map functions

	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};


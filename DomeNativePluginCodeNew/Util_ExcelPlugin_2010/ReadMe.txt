================================================================================
    MICROSOFT FOUNDATION CLASS LIBRARY : Util_ExcelPlugin_2010 Project Overview
===============================================================================

The application wizard has created this Util_ExcelPlugin_2010 application for
you.  This application not only demonstrates the basics of using the Microsoft
Foundation Classes but is also a starting point for writing your application.

This file contains a summary of what you will find in each of the files that
make up your Util_ExcelPlugin_2010 application.

Util_ExcelPlugin_2010.vcxproj
    This is the main project file for VC++ projects generated using an application wizard.
    It contains information about the version of Visual C++ that generated the file, and
    information about the platforms, configurations, and project features selected with the
    application wizard.

Util_ExcelPlugin_2010.vcxproj.filters
    This is the filters file for VC++ projects generated using an Application Wizard. 
    It contains information about the association between the files in your project 
    and the filters. This association is used in the IDE to show grouping of files with
    similar extensions under a specific node (for e.g. ".cpp" files are associated with the
    "Source Files" filter).

Util_ExcelPlugin_2010.h
    This is the main header file for the application.  It includes other
    project specific headers (including Resource.h) and declares the
    CUtil_ExcelPlugin_2010App application class.

Util_ExcelPlugin_2010.cpp
    This is the main application source file that contains the application
    class CUtil_ExcelPlugin_2010App.

Util_ExcelPlugin_2010.rc
    This is a listing of all of the Microsoft Windows resources that the
    program uses.  It includes the icons, bitmaps, and cursors that are stored
    in the RES subdirectory.  This file can be directly edited in Microsoft
    Visual C++. Your project resources are in 1033.

res\Util_ExcelPlugin_2010.ico
    This is an icon file, which is used as the application's icon.  This
    icon is included by the main resource file Util_ExcelPlugin_2010.rc.

res\Util_ExcelPlugin_2010.rc2
    This file contains resources that are not edited by Microsoft
    Visual C++. You should place all resources not editable by
    the resource editor in this file.

Util_ExcelPlugin_2010.reg
    This is an example .reg file that shows you the kind of registration
    settings the framework will set for you.  You can use this as a .reg
    file to go along with your application.

Util_ExcelPlugin_2010.idl
    This file contains the Interface Description Language source code for the
    type library of your application.


/////////////////////////////////////////////////////////////////////////////

The application wizard creates one dialog class and automation proxy class:

Util_ExcelPlugin_2010Dlg.h, Util_ExcelPlugin_2010Dlg.cpp - the dialog
    These files contain your CUtil_ExcelPlugin_2010Dlg class.  This class defines
    the behavior of your application's main dialog.  The dialog's template is
    in Util_ExcelPlugin_2010.rc, which can be edited in Microsoft Visual C++.

DlgProxy.h, DlgProxy.cpp - the automation object
    These files contain your CUtil_ExcelPlugin_2010DlgAutoProxy class.  This class
    is called the Automation proxy class for your dialog, because it
    takes care of exposing the Automation methods and properties that
    Automation controllers can use to access your dialog.  These methods
    and properties are not exposed from the dialog class directly, because
    in the case of a modal dialog-based MFC application it is cleaner and
    easier to keep the Automation object separate from the user interface.

/////////////////////////////////////////////////////////////////////////////

Other Features:

ActiveX Controls
    The application includes support to use ActiveX controls.

/////////////////////////////////////////////////////////////////////////////

Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named Util_ExcelPlugin_2010.pch and a precompiled types file named StdAfx.obj.

Resource.h
    This is the standard header file, which defines new resource IDs.
    Microsoft Visual C++ reads and updates this file.

/////////////////////////////////////////////////////////////////////////////

Other notes:

The application wizard uses "TODO:" to indicate parts of the source code you
should add to or customize.

If your application uses MFC in a shared DLL, you will need
to redistribute the MFC DLLs. If your application is in a language
other than the operating system's locale, you will also have to
redistribute the corresponding localized resources mfc110XXX.DLL.
For more information on both of these topics, please see the section on
redistributing Visual C++ applications in MSDN documentation.

/////////////////////////////////////////////////////////////////////////////


Meena Ganesh July 2015

1. This project was created to extract the classes from the Excel.exe
2. Create a MFC Application Project - Dialog based 
3. Go To Class wizard and add class "MFC Class from Typelib"
4. Search in the typelib list and select "MicroSoft Excel 14.0 Object Library<1.7> - 

5. Note this typelib appears because I had Office 2010 installed and this gets registered in the registry.
6. Select all classes.
7. We have copied all the "Applicable" classes that JNI would support for DOME and kept in excel_2010.h 
8. Testing is done using CApplication.h - note we had to comment the line
//#import "C:\\Program Files (x86)\\Microsoft Office\\Office14\\EXCEL.EXE" no_namespace



// Util_ExcelPlugin_2010.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CUtil_ExcelPlugin_2010App:
// See Util_ExcelPlugin_2010.cpp for the implementation of this class
//

class CUtil_ExcelPlugin_2010App : public CWinApp
{
public:
	CUtil_ExcelPlugin_2010App();

// Overrides
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CUtil_ExcelPlugin_2010App theApp;
// TypeConversions.h: utility functions for converting basic types to strings.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#ifndef DOME_TYPECONVERSIONS_H
#define DOME_TYPECONVERSIONS_H

#include <string>
#include <sstream>
#include <vector>

namespace DOME {
namespace Utilities {
namespace TypeConversions{

    template<class T>
    T fromString(const std::string& s) {
      std::istringstream is(s);
      T t;
      is >> t;
      return t;
    }

    std::string str(const char& t) {
      std::ostringstream s;
      s << t;
      return s.str();
    }

    std::string str(const int& t) {
      std::ostringstream s;
      s << t;
      return s.str();
    }

    std::string str(const float& t) {
      std::ostringstream s;
      s << t;
      return s.str();
    }

    std::string str(const double& t) {
      std::ostringstream s;
      s << t;
      return s.str();
    }

    std::string str(const bool& t) {
        std::ostringstream s;
        if (t)
            s << "true";
        else
            s << "false";
        return s.str();
    }

    std::string str(const std::vector<int>& v){
        std::stringstream s;
        for (unsigned int i=0; i<v.size(); i++)
            s << v[i] << " ";
        return s.str();
    }

    std::string str(const std::vector<double>& v){
        std::stringstream s;
        for (unsigned int i=0; i<v.size(); i++)
            s << v[i] << " ";
        return s.str();
    }

    std::string str(std::vector< std::vector<int> > v){
        std::stringstream s;
        if (v.size()>0) s << str(v[0]);
        for (unsigned int i=1; i<v.size(); i++)
            s << std::endl << str(v[i]);
        return s.str();
    }

    std::string str(std::vector< std::vector<double> > v){
        std::stringstream s;
        if (v.size()>0) s << str(v[0]);
        for (unsigned int i=1; i<v.size(); i++)
            s << std::endl << str(v[i]);
        return s.str();
    }

} // namespace TypeConversions
} // namespace Utilities
} // namespace DOME

#endif // DOME_TYPECONVERSIONS_H
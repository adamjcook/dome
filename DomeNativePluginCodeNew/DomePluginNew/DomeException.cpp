// DomeException.cpp: implementation of the DomeException class.
//
//////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "DomeException.h"

namespace DOME {
namespace DomePlugin {

    DomeException::DomeException(const std::string& msg) throw() :
        message_("DomeException: "+msg) {}

    DomeException::DomeException(const std::string& name, const std::string& msg) throw() :
        message_(name+": "+msg) {}

    const char* DomeException::what() const throw()
    {
        return const_cast<char*>(message_.c_str());
    }

    DomeException::DomeException(const char* file, long line, const std::string& msg) throw() :
        message_(makeMessage("DomeException",file,line,msg)) {}

    DomeException::DomeException(const std::string& name, const char* file,
        long line, const std::string& msg) throw() :
        message_(makeMessage(name,file,line,msg)) {}

    std::string DomeException::makeMessage(const std::string& name, const char* file, long line, const std::string& msg)
    {
        std::stringstream message;
        message << name << ": " << file << "(" << line << ")";
        if (msg.size())
            message << ": " << msg;
        return message.str();
    }

} // namespace DomePlugin
} // namespace DOME

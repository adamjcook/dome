// CATIAModel.cpp: implementation of the CATIAModel class.
//
//////////////////////////////////////////////////////////////////////

#define _WIN32_WINNT 0x0400 // required for multithreaded COM

// Plugin
#include "CatiaIncludes.h"
#include "CatiaData.h"
#include "CatiaModel.h"
//#include "CATDocumentsInSession.h" // CATLISTP(CATDocument) and CATLISTV(CATUnicodeString)


namespace DOME {
namespace CATIAPlugin {


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CATIAModel::CATIAModel (char* filename, bool bIsVisible)
	: m_Filename (NULL), m_pSession (NULL), m_pDoc (NULL),
	  m_bIsVisible (bIsVisible), m_bModelLoaded (false),
	  m_UserLibraryManagers (NULL), m_NumUserLibraries(0)
{
	if (filename != NULL) {
		m_Filename = new char[strlen(filename)+1];
		strcpy (m_Filename, filename);
	}

	m_pPart = NULL;
	m_pPartSpec = NULL;

	m_pCatiaApp = NULL;
	m_pDoc = NULL;
	m_pPart = NULL;
}

CATIAModel::~CATIAModel()
{
	unloadModel(-1); //so that the session is not deleted

	int size = m_Data.size();
	for (int i = 0; i < size; i++) {
		delete m_Data[i];
		m_Data[i] = NULL;
	}

	size = m_Files.size();
	for (i = 0; i < size; i++) {
		delete m_Files[i];
		m_Files[i] = NULL;
	}

	delete m_Filename;

	// destroy user library structures
	for (i = 0; i < m_NumUserLibraries; i++) {
		delete m_UserLibraryManagers[i];
	}
	delete []m_UserLibraryManagers;
}


/////////////////////////////////////////////////////
// Check if model has been loaded
/////////////////////////////////////////////////////
bool CATIAModel::isModelLoaded()
{
	return m_bModelLoaded;  
}


/////////////////////////////////////////////////////
// Load the model
/////////////////////////////////////////////////////
int CATIAModel::loadModel() 
{

	//------------------------------------------------------------------
	//1 - Creates Catia Process
	//------------------------------------------------------------------
 

#ifdef _WIN32_WINNT
	// enable multithreaded COM library
	CoInitializeEx(NULL, COINIT_MULTITHREADED);
#endif

	// Get CLSID for Catia
	CLSID	clsid;
	HRESULT hr;
    //CATIAApplication* myCatia;
	//CAT_VARIANT_BOOL oVisible;

    // get CLSID for Catia
    hr = CLSIDFromProgID(L"CATIA.Application", &clsid);

	if(FAILED(hr)) { 
		cout << "Catia: CLSIDFromProgID() failed." << endl;
		return -1;
	}

    IUnknown* pUnk = NULL;

	hr = GetActiveObject (clsid, NULL, &pUnk);
    if (FAILED (hr)) {
		 hr = CoCreateInstance(clsid, NULL, CLSCTX_LOCAL_SERVER, IID_IUnknown, (void **)&pUnk);
	     if(FAILED(hr)) { 
				cout << "Catia: Catia not registered properly." << endl;
				return -1;
		 }

	}

     //hr = CoGetClassObject(clsid, CLSCTX_LOCAL_SERVER, NULL, IID_IClassFactory, (void **)&pUnk);

/*    hr = CoCreateInstance(clsid, NULL, CLSCTX_LOCAL_SERVER, IID_IUnknown, (void **)&pUnk);
    if(FAILED(hr) || pUnk == NULL) { 
		cout << "Catia: Catia not registered properly." << endl;
		return -1;
	}
*/
	
    //IDispatch* pDisp;
    //hr = pUnk->QueryInterface(IID_IDispatch, (void **)&pDisp);

    hr = pUnk->QueryInterface(IID_CATIAApplication, (void **)&m_pCatiaApp);

    if(FAILED(hr) || m_pCatiaApp == NULL) { 
		cout << "Catia: didn't get CATIAApplication." << endl;
		return -1;
	}

	pUnk->Release(); 
	m_pCatiaApp->put_DisplayFileAlerts(CAT_VARIANT_FALSE);

	//------------------------------------------------------------------
	//2 - Opens the Part document
	//------------------------------------------------------------------
	
	// load the document and initializes it
	CATIADocuments * m_Documents = NULL;
	CATUnicodeString myFileName = m_Filename; // "D:\\Part2.CATPart";
	CATBSTR DocumentName;          // = m_Filename;
	myFileName.ConvertToBSTR(&DocumentName);

	//CATString DocumentName = m_Filename;
	//CATIADocument* myDoc;

	hr = m_pCatiaApp->get_Documents(m_Documents);
    if(FAILED(hr) || m_Documents == NULL) { 
		cout << "Catia: didn't get Document Collection." << endl;
		return -1;
	}

	hr = m_Documents->Open(DocumentName, m_pDoc);

    if(FAILED(hr) || m_pDoc == NULL) { 
		cout << "Catia: didn't open the Document: " << m_Filename << endl;
		return -1;
	}

	m_Documents->Release(); 

	//-----------------------------


	//-----------------------------

/*	CATDocument *pmyCATDoc;
    hr = myDoc->QueryInterface(IID_CATDocument, (void**) &pmyCATDoc);
    if(FAILED(hr)) { 
		cout << "Catia: didn't get the CAT Document." << endl;
		return -1;
	} else cout << "Catia: get the CAT Document." << endl;
*/
/*	CATIEditor *pmyEditor = NULL;
    hr = myDoc->QueryInterface(IID_CATIEditor, (void**) &pmyEditor);
    if(FAILED(hr)) { 
		cout << "Catia: didn't get the CAT Editor." << endl;
    if(pmyEditor==NULL) { 
		cout << "Catia: didn't get the CAT Editor.NULL" << endl;
		return -1;
	} else cout << "Catia: get the CAT Editor.NOT NULL" << endl;

		return -1;
	} else cout << "Catia: get the CAT Editor." << endl;
*/

	CATIAPartDocument *pPartDoc = NULL;
    hr = m_pDoc->QueryInterface(IID_CATIAPartDocument, (void**) &pPartDoc);
    if(FAILED(hr) || pPartDoc == NULL) { 
		cout << "Catia: didn't get the Part Document." << endl;
		return -1;
	}

	hr = pPartDoc->get_Part(m_pPart);
    if(FAILED(hr) || m_pPart == NULL) { 
		cout << "Catia: didn't get part." << endl;
		return -1;
    }

/*	CATIABodies *pBodies;
	hr = oPart->get_Bodies(pBodies);
    if(FAILED(hr)) { 
		cout << "Catia: didn't get bodies." << endl;
		return -1;
    }
*/

/*    CATIPrtPart *mypPart = NULL;
    hr = oPart->QueryInterface(IID_CATIPrtPart, (void**) &mypPart);
    if(FAILED(hr)) { 
		cout << "Catia: didn't convert to CAA part!!" << endl;
		return -1;
	} else 	cout << "Catia: Convert to CAA part!!" << endl;
*/


/*    CATIPartRequest *mypPart = NULL;

    hr = oPart->QueryInterface(IID_CATIPartRequest, (void**) &mypPart);
*/

/*    CATIParmPublisher *mypPart = NULL;

    hr = oPart->QueryInterface(IID_CATIParmPublisher, (void**) &mypPart);

    if(FAILED(hr)) { 
		cout << "Catia: didn't convert to CAA part." << endl;
		return -1;
	} else 	cout << "Catia: Convert to CAA part." << endl;
*/
    

/*	CATInit * pDocAsInit = NULL;
	hr = myDoc->QueryInterface(IID_CATInit, (void**)&pDocAsInit);
	if ((FAILED(hr)))
	{
		CATIA_ERROR("CatiaModel::loadModel: failed to get the CATInit interface");
		return -1;
	} else {
		if ((NULL == pDocAsInit)) cout << "Catia: CATInit interface is NULL!" << endl;
	    else cout << "Catia: get CATInit interface!" << endl;
	}
*/

/*	myCatia->get_Visible(oVisible);
	if(oVisible) cout << "Catia: Visible!" << endl;
	else cout << "Catia: Invisible!" << endl;

	myCatia->put_Visible(CAT_VARIANT_FALSE);

	myCatia->get_Visible(oVisible);
	if(oVisible) cout << "Catia: Visible!" << endl;
	else cout << "Catia: Invisible!" << endl;
*/

	// make connections to the part parameters
	_createConnections();

	// set model loaded flag
	m_bModelLoaded = true;
	CATIA_DEBUG("CATIA model "<<m_Filename<<" successfully loaded.");


	//------------------------------------------------------------------
	//4 - Display the part in a visualization window
	//------------------------------------------------------------------

	if (!m_bIsVisible)  m_pCatiaApp->put_Visible(CAT_VARIANT_FALSE);
	else m_pCatiaApp->put_Visible(CAT_VARIANT_TRUE);


	return 0;


}



/////////////////////////////////////////////////////
// Unload the model
/////////////////////////////////////////////////////
void CATIAModel::unloadModel(int numModels) 
{

	CATIA_DEBUG("Start to unload model!");
	if (isModelLoaded())
	{
		_destroyConnections();

		CATTry 
		{	 
			if (numModels == 1 && m_pCatiaApp != NULL)
			{
				m_pCatiaApp->Quit();
			}
		}
		// This block treats every other exception
		CATCatch(CATError,error)    {
			cerr << " Error: " << error->GetMessageText() <<endl; 
			Flush(error);
		}
		CATEndTry;

/*		CATTry 
		{	 
			if (m_pDoc != NULL)
			{
				::CATUnLockDocument(*m_pDoc); // this will remove document when last lock is released
				//CATDocumentServices::Remove(*m_pDoc); // not needed now; can use again when multiple instances of file really created
			}

			if (numModels == 1 && m_pSession != NULL)  //this is the last model being unloaded
			{
				::Delete_Session(m_pSessionName); //so delete the session
				CATIA_DEBUG("CATIA model "<<m_Filename<<" deleted session " << m_pSessionName);
			}
		}
		// This block treats every other exception
		CATCatch(CATError,error)    {
			cerr << " Error: " << error->GetMessageText() <<endl; 
			Flush(error);
		}
		CATEndTry;
*/
		m_pSession = NULL;
		m_pDoc = NULL;
		m_pPart = NULL;
		m_pPartSpec = NULL;

#ifdef _WIN32_WINNT
		CoUninitialize();
#endif

		CATIA_DEBUG("CATIA model "<<m_Filename<<" successfully unloaded.");
		
		m_bModelLoaded = false;
	}
}


/////////////////////////////////////////////////////
// Batch execute changes to the model
/////////////////////////////////////////////////////
int CATIAModel::execute() 
{

	if(m_pCatiaApp == NULL)
	{
		CATIA_DEBUG ("execute: Catia has not been started");
		return -1;
	}
	else
	{
		if (!m_bIsVisible)  m_pCatiaApp->put_Visible(CAT_VARIANT_FALSE);
		else m_pCatiaApp->put_Visible(CAT_VARIANT_TRUE);
	}
	
	if (m_pPart == NULL)
	{
		CATIA_DEBUG ("execute: Model has not been loaded");
	}
	else
	{

		//printf("Address of m_pPartSpec: %d\n", &m_pPartSpec);

		CATTry  {
			m_pPart -> Update();
		}
		// This block is specific for Update Errors
		CATCatch(CATMfErrUpdate,error)	 {
			cerr << " Update Error: CatiaPlugin " << (error-> GetDiagnostic()).ConvertToChar() << endl; 
			Flush(error) ; 
			return E_FAIL; 
		}
		// This block is specific for Other Mechanical Modeler Errors
		CATCatch(CATMfError,error)	 {
			cerr << " Geometric Error: " << error-> GetMessageText() << endl; 
			Flush(error) ; 
			return E_FAIL; 
		}
		// This block treats every other exception
		CATCatch(CATError,error)    {
			cerr << " Error: " << error->GetMessageText() <<endl; 
			Flush(error);
			return E_FAIL; 
		}
		CATEndTry;

		// execute the user libraries
		for (int i = 0; i < m_NumUserLibraries; i++)
		{
			m_UserLibraryManagers[i]->run ();
			//m_UserLibraryManagers[i]->run (m_pPart, m_pPartSpec);
		}


		// save the results to any existingfile parameters
		int size = m_Files.size();
		for (i = 0; i < size; i++) 
		{
			m_Files[i]->save ();
		} 
	}

	return 0;
}


/////////////////////////////////////////////////////
// Connect to the part parameters
/////////////////////////////////////////////////////
void CATIAModel::_createConnections()
{
	int size = m_Data.size();
	for (int i=0; i<size; i++)
	{
		m_Data[i]->connect(m_pPart);
	}

	size = m_Files.size();
	for (i=0; i<size; i++)
	{
		m_Files[i]->connect(m_pDoc);
	}
}


/////////////////////////////////////////////////////
// Disconnect the part parameters
/////////////////////////////////////////////////////
void CATIAModel::_destroyConnections()
{
	int size = m_Data.size();
	for (int i=0; i<size; i++)
	{
		m_Data[i]->disconnect();
	}

	size = m_Files.size();
	for (i=0; i<size; i++)
	{
		m_Files[i]->disconnect();
	}
}


/////////////////////////////////////////////////////
// Create a real parameter
/////////////////////////////////////////////////////
CATIAReal* CATIAModel::createReal(char *name)
{
	CATIAReal* real = new CATIAReal(name);
	m_Data.push_back(real);
	return real;
}


/////////////////////////////////////////////////////
// Create an integer parameter
/////////////////////////////////////////////////////
CATIAInteger* CATIAModel::createInteger(char *name)
{
	CATIAInteger* integer = new CATIAInteger(name);
	m_Data.push_back(integer);
	return integer;
}


/////////////////////////////////////////////////////
// Create a string parameter
/////////////////////////////////////////////////////
CATIAString* CATIAModel::createString(char *name)
{
	CATIAString* str = new CATIAString(name);
	m_Data.push_back(str);
	return str;
}


/////////////////////////////////////////////////////
// Create a boolean parameter
/////////////////////////////////////////////////////
CATIABoolean* CATIAModel::createBoolean(char *name)
{
	CATIABoolean* boolean = new CATIABoolean(name);
	m_Data.push_back(boolean);
	return boolean;
}


/////////////////////////////////////////////////////
// Create a file parameter
/////////////////////////////////////////////////////
CATIAFile* CATIAModel::createFile(char *fileName, char *fileType)
{
	CATIAFile* file = new CATIAFile(fileName, fileType);
	m_Files.push_back(file);
	return file;
}


/////////////////////////////////////////////////////
// Create a vector parameter
/////////////////////////////////////////////////////
CATIAVector* CATIAModel::createVector(char *name)
{
	CATIAVector* vec = new CATIAVector(name);
	m_Data.push_back(vec);
	return vec;
}



/////////////////////////////////////////////////////
// Create a string parameter for a DLL call
/////////////////////////////////////////////////////
CATIAString* CATIAModel::createStringUserLib(char *name, char *LibraryName, int argIndex)
{
	CATIAString* str = NULL;
	
	// find an existing library struct add the parameter to it
	for (int i = 0; i < m_NumUserLibraries; i++)
	{
		if (strcmp(m_UserLibraryManagers[i]->getName(), LibraryName) == 0)
		{
			// create new parameter
			str = new CATIAString(name, m_UserLibraryManagers[i]);
			m_Data.push_back(str);

			// add the parameter to the DLL struct
			m_UserLibraryManagers[i]->addParameter (argIndex, str);
		}
	}

	return str;
}


/////////////////////////////////////////////////////
// Create a real parameter for a DLL call
/////////////////////////////////////////////////////
CATIAReal* CATIAModel::createRealUserLib(char *name, char *LibraryName, int argIndex)
{
	CATIAReal* real = NULL;
	
	// find an existing library struct add the parameter to it
	for (int i = 0; i < m_NumUserLibraries; i++)
	{
		if (strcmp(m_UserLibraryManagers[i]->getName(), LibraryName) == 0)
		{
			// create new parameter
			real = new CATIAReal(name, m_UserLibraryManagers[i]);
			m_Data.push_back(real);

			// add the parameter to the DLL struct
			m_UserLibraryManagers[i]->addParameter (argIndex, real);
		}
	}

	return real;
}


/////////////////////////////////////////////////////
// Create a real parameter for a DLL call
/////////////////////////////////////////////////////
CATIAInteger* CATIAModel::createIntegerUserLib(char *name, char *LibraryName, int argIndex)
{
	CATIAInteger* integer = NULL;
	
	// find an existing library struct add the parameter to it
	for (int i = 0; i < m_NumUserLibraries; i++)
	{
		if (strcmp(m_UserLibraryManagers[i]->getName(), LibraryName) == 0)
		{
			// create new parameter
			integer = new CATIAInteger(name, m_UserLibraryManagers[i]);
			m_Data.push_back(integer);

			// add the parameter to the DLL struct
			m_UserLibraryManagers[i]->addParameter (argIndex, integer);
		}
	}

	return integer;
}


/////////////////////////////////////////////////////
// Create a real parameter for a DLL call
/////////////////////////////////////////////////////
CATIABoolean* CATIAModel::createBooleanUserLib(char *name, char *LibraryName, int argIndex)
{
	CATIABoolean* boolean = NULL;
	
	// find an existing library struct add the parameter to it
	for (int i = 0; i < m_NumUserLibraries; i++)
	{
		if (strcmp(m_UserLibraryManagers[i]->getName(), LibraryName) == 0)
		{
			// create new parameter
			boolean = new CATIABoolean(name, m_UserLibraryManagers[i]);
			m_Data.push_back(boolean);

			// add the parameter to the DLL struct
			m_UserLibraryManagers[i]->addParameter (argIndex, boolean);
		}
	}

	return boolean;
}


/////////////////////////////////////////////////////
// Create a string parameter for a DLL call
/////////////////////////////////////////////////////
CATIAVector* CATIAModel::createVectorUserLib(char *name,
											 char *LibraryName, unsigned int argIndex)
{
	CATIAVector* vec = NULL;

	// find an existing library struct add the parameter to it
	for (int i = 0; i < m_NumUserLibraries; i++)
	{
		if (strcmp(m_UserLibraryManagers[i]->getName(), LibraryName) == 0)
		{
			vec = new CATIAVector(name, m_UserLibraryManagers[i]);
			m_Data.push_back(vec);

			// add the parameter to the DLL struct
			m_UserLibraryManagers[i]->addParameter (argIndex, vec);
		}
	}

	return vec;
}


/////////////////////////////////////////////////////
// Setup a new DLL structure
/////////////////////////////////////////////////////
void CATIAModel::setupUserLibrary (char *name, unsigned int numParameters)
{
	// This method uses an inefficient but simple way to add a new structure.
	// Ideally a pool would be created in advance. However, this
	// operation will happen only when the model is first loaded
	// and therefore unlikely to be a performance issue. For this reason,
	// a simple but somewhat dumb algorithm is fine.

	// create a new container for the DLL manager structures
	UserLibraryManager **newUserLibraryManagers = new UserLibraryManager*[m_NumUserLibraries+1];
	
	// copy the old managers
	for (int i = 0; i < m_NumUserLibraries; i++)
	{
		newUserLibraryManagers[i] = m_UserLibraryManagers[i];
	}

	// create a new manager
	m_NumUserLibraries++;
	newUserLibraryManagers[i] = new UserLibraryManager (name, numParameters, this);
	m_UserLibraryManagers = newUserLibraryManagers;
}



/////////////////////////////////////////////////////
// DLL Manager constructor and deconstructor
/////////////////////////////////////////////////////

UserLibraryManager::UserLibraryManager (char *name, unsigned int numParameters, CATIAModel *pmodel)
: m_LibraryName (NULL), m_Parameters (NULL), m_NumParameters(0), m_hDLL(NULL), m_CBD(NULL), m_CATIADomeInterface(NULL), m_model(NULL)
{
	if (name != NULL)
	{
		m_LibraryName = new char[strlen(name)+1];
		strcpy (m_LibraryName, name);
	}

	if (numParameters > 0)
	{
		m_NumParameters = numParameters;
		m_Parameters = new CATIAData*[numParameters];
	}
	
	if (pmodel != NULL) {
		m_model = pmodel;
	}

	// load the DLL and get handles to its functions
/*	m_hDLL = LoadLibrary(name);
	if (m_hDLL == NULL)
	{
	  CATIA_ERROR ("Failed to load the user library: " << name << " not found" << endl);
	}
	else
	{
	   m_funcRun = (FUNC_RUN)GetProcAddress(m_hDLL, (LPSTR)"DOME_Run");
	   m_funcSetItem = (FUNC_SET_ITEM)GetProcAddress(m_hDLL, (LPSTR)"DOME_SetItem");
	   m_funcGetItem = (FUNC_GET_ITEM)GetProcAddress(m_hDLL, (LPSTR)"DOME_GetItem");
	   m_funcSetItemDoubleArray = (FUNC_SET_ITEM_DOUBLEARRAY)
		   GetProcAddress(m_hDLL, (LPSTR)"DOME_SetItem_DoubleArray");
	   m_funcGetItemDoubleArray = (FUNC_GET_ITEM_DOUBLEARRAY)
		   GetProcAddress(m_hDLL, (LPSTR)"DOME_GetItem_DoubleArray");

	   if (!(m_funcRun && m_funcSetItem && m_funcGetItem &&
		     m_funcSetItemDoubleArray && m_funcGetItemDoubleArray))
	   {
		  // handle the error
		  FreeLibrary(m_hDLL);
		  m_hDLL = NULL;
		  CATIA_ERROR ("Failed to load the user library " << name << endl);
		  CATIA_ERROR ("One or more functions not found." << endl);
	   }
	}
*/
}


UserLibraryManager::~UserLibraryManager ()
{
	// unload the DLL
	if (m_hDLL != NULL) {
		FreeLibrary(m_hDLL);
	}

	delete[] m_LibraryName;

	// just delete the parameter container:
	// the model takes care of deleting individual parameters
	delete[] m_Parameters;
}


//////////////////////////////////////////////////////////////////
// add a parameter to the list of parameters for this library
//////////////////////////////////////////////////////////////////
void UserLibraryManager::addParameter (int index, CATIAData *data)
{
	if (index < m_NumParameters) {
		m_Parameters[index] = data;
	}
	else {
		CATIA_DEBUG ("UserLibraryManager::addParameter: index out of range" << endl);
	}
}

/////////////////////////////////////////////////////
// get the library name
/////////////////////////////////////////////////////
const char *UserLibraryManager::getName () const
{
	return m_LibraryName;
}


/////////////////////////////////////////////////////
// set the value of a parameter
/////////////////////////////////////////////////////
void UserLibraryManager::setValue (const CATIAData *param, CATVariant va)
{
    HRESULT hr;

	//** If the automation interface of the corresponding dll has not been queried, do it first.
	if(m_CATIADomeInterface == NULL) {
			//cout << "Catia: UserLibraryManager::setValue. m_CATIADomeInterface == NULL" << endl;
			if (m_model == NULL ) {
				cout << "Catia: UserLibraryManager::setValue. No model" << endl;
				return;
			} else if (m_model->m_pPart == NULL) {
				cout << "Catia: UserLibraryManager::setValue. No part" << endl;
				return;
			} 
			CATUnicodeString compName = m_LibraryName;
			CATBSTR componentName;          
			compName.ConvertToBSTR(&componentName);
			hr = m_model->m_pPart->GetItem(componentName, m_CBD);
			if(FAILED(hr) || m_CBD == NULL ) {
				cout << "Catia: UserLibraryManager::setValue. Failed to create " << m_LibraryName << " component in CATIA process." << endl;
				return;
			}

			hr = m_CBD->QueryInterface(IID_CATIADomeInterface, (void**) &m_CATIADomeInterface);
			if(FAILED(hr) || m_CATIADomeInterface == NULL) {
				cout << "Catia:  UserLibraryManager::setValue. Failed to create Automation Interface for " << m_LibraryName << " component in CATIA process." << endl;
				return;
			}

	}

	for (int i = 0; i < m_NumParameters && (m_Parameters[i] != param); i++)
	{
	}

	if (m_Parameters[i] == param) {
	    //** call automation interface to set value here
		//cout << "Catia: UserLibraryManager::setValue. m_CATIADomeInterface != NULL" << endl;

		hr = m_CATIADomeInterface->SetDllItem(i+2, va);
		if(!FAILED(hr)) cout << "Catia: succeeded to set item " << i << " to " << m_LibraryName << endl;
		else cout << "Catia: failed to set item " << i << " to " << m_LibraryName << endl;
		//m_funcSetItem (i+2, value);
		
		//** for debug
		//**if(i==4) printf ("CATIA Debug: SetValue %d as %d\n",i+2, value);
		//**else printf ("CATIA Debug: SetValue %d as %s\n",i+2, value);

	}
	else {
		CATIA_DEBUG ("UserLibraryManager::setValue: could not find parameter" << endl);
	}
}


/////////////////////////////////////////////////////
// get the value of a parameter
/////////////////////////////////////////////////////
void* UserLibraryManager::getValue (const CATIAData *param) const
{
	void* value = NULL;

	for (int i = 0; i < m_NumParameters && (m_Parameters[i] != param); i++)
	{
	}

	if (m_Parameters[i] == param) {
		value = m_funcGetItem (i+2);
	}
	else {
		CATIA_DEBUG ("UserLibraryManager::getValue: could not find parameter" << endl);
	}

	return value;
}


/////////////////////////////////////////////////////
// set the value of a double array parameter
/////////////////////////////////////////////////////
void UserLibraryManager::setValueDoubleArray (const CATIAData *param, vector<double> values)
{
	for (int i = 0; i < m_NumParameters && (m_Parameters[i] != param); i++)
	{
	}

	if (m_Parameters[i] == param) {
		//** todo: this will be changed later
		m_funcSetItemDoubleArray (i+2, values);
	}
	else {
		CATIA_DEBUG ("UserLibraryManager::setValueDoubleArray: could not find parameter"
					 << endl);
	}
}


/////////////////////////////////////////////////////
// get the value of a double array parameter
/////////////////////////////////////////////////////
vector<double> UserLibraryManager::getValueDoubleArray (const CATIAData *param) const
{
	vector<double> value;

	if(m_CATIADomeInterface == NULL) return value;

	for (int i = 0; i < m_NumParameters && (m_Parameters[i] != param); i++)
	{
	}

	if (m_Parameters[i] == param) {
		//value = m_funcGetItemDoubleArray (i+2);

/*		double *arr = new double[1];
		CATSafeArrayVariant* csa = BuildSafeArrayVariant(arr, 1);

		HRESULT hr = m_CATIADomeInterface->GetDllItemArray(0, *csa);

		cout << "Catia: get out of CATIA! HRESULT is: " << hr << endl;
		//** convert CATSafeArrayVariant to vector via array
		long arraySize =  ConvertSafeArrayVariant(csa, (double*)NULL, 0);   //GetVariantArraySize(csa); 

		cout << "Catia: Size of CATSafeArrayVariant: " << arraySize << endl;

		if (arraySize > 0) {
			double *dblArr = new double[arraySize];
			if (arraySize == ConvertSafeArrayVariant(csa, dblArr, arraySize)) {
				    for (int i = 0; i < arraySize; i++)
					{
						value.push_back(dblArr[i]);
					}
			} else cout << "Catia: UserLibraryManager::getValueDoubleArray: didn't get array result from CATIA." << endl;

		} else cout << "Catia: UserLibraryManager::getValueDoubleArray: didn't get array result from CATIA." << endl;
*/

		//** create a variant with safearray
		SAFEARRAY *psa; 
		SAFEARRAYBOUND rgsabound[1];
		rgsabound[0].lLbound = 0;
		rgsabound[0].cElements = 1;
		psa = SafeArrayCreate(VT_R8, 1, rgsabound); 

		VARIANT va; 
		va.vt = VT_ARRAY | VT_R8;
		va.parray = psa;

		HRESULT hr = m_CATIADomeInterface->GetDllItem(i+2,va);
		if(FAILED (hr)) return value;

		//cout << "Catia: Get array results" << endl;

		double *dbl;
		long LBound = 0;
		long UBound = 0;

		SafeArrayAccessData(va.parray, (void HUGEP **) &dbl);
		//cout << "Catia: get array"  << endl;
		SafeArrayGetLBound(va.parray, 1, &LBound);
		//cout << "Catia: LBound is: " << LBound << endl;
		SafeArrayGetUBound(va.parray, 1, &UBound);
		//cout << "Catia: UBound is: " << UBound << endl;

		for(long index=0;index<UBound - LBound +1;index++)         
		{           
			//if(!FAILED(SafeArrayGetElement(va.parray,&index,&dbl)))
			{
				value.push_back(dbl[index]); 
				//cout << "Catia: result element is: " << value[index] << endl;
			}
			
		}

/*
		//** When in IDL, the direction of parameter iValue is "out", use the following code
		VARIANT * pva;
		pva = &va;

		HRESULT hr = m_CATIADomeInterface->GetDllItem(0,pva);
		//if(FAILED (hr)) return value;

		cout << "Catia: Get array results" << endl;

		double *dbl;
		long LBound = 0;
		long UBound = 0;

		SafeArrayAccessData((*pva).parray, (void HUGEP **) &dbl);
		cout << "Catia: get array"  << endl;
		SafeArrayGetLBound((*pva).parray, 1, &LBound);
		cout << "Catia: LBound is: " << LBound << endl;
		SafeArrayGetUBound((*pva).parray, 1, &UBound);
		cout << "Catia: UBound is: " << UBound << endl;

		for(long index=0;index<UBound - LBound +1;index++)         
		{           
			//if(!FAILED(SafeArrayGetElement((*pva).parray,&index,&dbl)))
			{
				value.push_back(dbl[index]); 
				cout << "Catia: result element is: " << value[index] << ":  " << dbl << endl;
			}
			
		}

	*/	

	}
	else {
		CATIA_DEBUG ("UserLibraryManager::getValueDoubleArray: could not find parameter"
					 << endl);
	}

	return value;
}


/////////////////////////////////////////////////////
// execute the library
/////////////////////////////////////////////////////
void UserLibraryManager::run()  //run (CATIPrtPart_var pPartPart, CATISpecObject_var pPartObj)
{
	if(m_CATIADomeInterface != NULL) 
	{
		HRESULT hr;

		if (m_model == NULL ) {
			cout << "Catia: UserLibraryManager::run. No model" << endl;
			return;
		} else if (m_model->m_pPart == NULL) {
			cout << "Catia: UserLibraryManager::run. No part" << endl;
			return;
		} 
		
		IDispatch* pDisp;
        
		hr = m_model->m_pPart->QueryInterface(IID_IDispatch, (void **)&pDisp); 
		CATVariant va;
		va.vt = VT_DISPATCH;
		va.pdispVal = pDisp;

		m_CATIADomeInterface->SetDllItem(0, va);

		// set the part objects
		//m_funcSetItem (0, (void*)&pPartPart);
		//m_funcSetItem (1, (void*)&pPartObj);

		//** for debug 
		//printf("Address of PartPart: %d\n", &pPartPart);
		//printf("Address of PartObj: %d\n", &pPartPart);

		// call the DLL's run function
		cout << "Catia: start to run third-party function - " << m_LibraryName << endl;
		cout << "Catia: running ......" << endl;
		m_CATIADomeInterface->Run();
		cout << "Catia: finished third-party function - " << m_LibraryName << endl;
	}
}


} //namespace CATIAPlusin
} //DOME
//----------------------------------------------------------------------------
// CATIADelegate.cpp
//
// Purpose: unwraps command, data objects and data values supplied by
// the caller. Instantiates model and data objects. Delegates work
// to the model and data objects.
//
// *********** PLUGIN AUTHOR MODIFIES EVERYTHING BELOW THIS LINE ************
// ***********					  AS NEEDED                      ************
//----------------------------------------------------------------------------


// catia function prototypes
#include "CATIAIncludes.h"

// data and model classes
#include "CATIAData.h"
#include "CATIAModel.h"
using namespace DOME::CATIAPlugin;

// java native interface stuff
#include <jni.h>
#include <iostream.h>
#include "jnihelper.h"

// hack to prevent forward defintion from causing ambiguity error
#define DELEGATE_CPP

// definitions for this class
#include <vector.h>
#include "CATIAPluginCaller.h"
#include "CATIADelegate.h"



/////////////////////////////////////////////////////////////////////
// invoke all functions that return an object (casted to a long)
/////////////////////////////////////////////////////////////////////
long CATIADelegate::callObjectFunctions (JNIEnv* env, jobjectArray args, 
										 unsigned int iNumArgs,
										 long iObjectPtr,
										 unsigned int iFunctionIndex)
{
	// call the appropriate function
	switch (iFunctionIndex)
	{
	case CATIAPluginCaller_MODEL_INIT:
		iObjectPtr = (long) initializeModel (env, args, iNumArgs);
		break;

	case CATIAPluginCaller_MODEL_CREATE_REAL:
	case CATIAPluginCaller_MODEL_CREATE_INT:
	case CATIAPluginCaller_MODEL_CREATE_STRING:
	case CATIAPluginCaller_MODEL_CREATE_BOOL:
	case CATIAPluginCaller_MODEL_CREATE_LENGTH:
	case CATIAPluginCaller_MODEL_CREATE_ANGLE:
	case CATIAPluginCaller_MODEL_CREATE_MASS:
	case CATIAPluginCaller_MODEL_CREATE_VOLUME:
	case CATIAPluginCaller_MODEL_CREATE_DENSITY:
	case CATIAPluginCaller_MODEL_CREATE_AREA:
		iObjectPtr = (long) createModelObject (env, args, iNumArgs, iObjectPtr, iFunctionIndex);
		break;

	case CATIAPluginCaller_MODEL_CREATE_VECTOR:
		CATIA_DEBUG ("CATIA does not support vector types" << endl);
		break;

	case CATIAPluginCaller_MODEL_CREATE_VECTOR_USERLIB:
		iObjectPtr = (long) createUserLibraryVectorObject (env, args, iNumArgs, iObjectPtr);
		break;

	case CATIAPluginCaller_MODEL_CREATE_REAL_USERLIB:
	case CATIAPluginCaller_MODEL_CREATE_STRING_USERLIB:
	case CATIAPluginCaller_MODEL_CREATE_INTEGER_USERLIB:
	case CATIAPluginCaller_MODEL_CREATE_BOOLEAN_USERLIB:
		iObjectPtr = (long) createUserLibraryModelObject (env, args, iNumArgs, iObjectPtr,
														  iFunctionIndex);
		break;

	case CATIAPluginCaller_MODEL_CREATE_FILE:
		iObjectPtr = (long) createFileObject (env, args, iNumArgs, iObjectPtr);
		break;

	default:
		CATIA_DEBUG ("callObjectFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return iObjectPtr;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return void
/////////////////////////////////////////////////////////////////////
void CATIADelegate::callVoidFunctions (JNIEnv* env, jobjectArray args, 
									   unsigned int iNumArgs,
									   long iObjectPtr,
									   unsigned int iFunctionIndex)
{
	// call the appropriate function
	switch (iFunctionIndex)
	{
	case CATIAPluginCaller_MODEL_LOAD:
		loadModel (env, iObjectPtr);
		break;

	case CATIAPluginCaller_MODEL_UNLOAD:
		unloadModel (env, args, iNumArgs, iObjectPtr);
		break;

	case CATIAPluginCaller_MODEL_DESTROY:
		destroyModel (env, iObjectPtr);
		break;

	case CATIAPluginCaller_MODEL_SETUP_USERLIB:
		setupUserLibrary (env, args, iNumArgs, iObjectPtr);
		break;

	case CATIAPluginCaller_REAL_SETVALUE:
	case CATIAPluginCaller_INT_SETVALUE:
	case CATIAPluginCaller_STRING_SETVALUE:
	case CATIAPluginCaller_LENGTH_SETVALUE:
	case CATIAPluginCaller_ANGLE_SETVALUE:
	case CATIAPluginCaller_MASS_SETVALUE:
	case CATIAPluginCaller_VOLUME_SETVALUE:
	case CATIAPluginCaller_DENSITY_SETVALUE:
	case CATIAPluginCaller_AREA_SETVALUE:
		setValue (env, args, iNumArgs, iObjectPtr, iFunctionIndex);
		break;

	case CATIAPluginCaller_VECTOR_SETVALUE:
		set1DArrayValues (env, args, iNumArgs, iObjectPtr, iFunctionIndex);
		break;

	default:
		CATIA_DEBUG ("callVoidFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return an integer
/////////////////////////////////////////////////////////////////////
int CATIADelegate::callIntegerFunctions (JNIEnv* env, jobjectArray args, 
										 unsigned int iNumArgs,
										 long iObjectPtr,
										 unsigned int iFunctionIndex)
{
	int value = 0;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	case CATIAPluginCaller_MODEL_EXECUTE:
		value = executeModel (env, iObjectPtr);
		break;

	case CATIAPluginCaller_INT_GETVALUE:
		value = getIntegerValue (env, iObjectPtr);
		break;

	default:
		CATIA_DEBUG ("callIntegerFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return value;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a boolean
/////////////////////////////////////////////////////////////////////
bool CATIADelegate::callBooleanFunctions (JNIEnv* env, jobjectArray args, 
										  unsigned int iNumArgs,
										  long iObjectPtr,
										  unsigned int iFunctionIndex)
{
	bool value = false;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	case CATIAPluginCaller_BOOL_GETVALUE:
		value = getBooleanValue (env, iObjectPtr);
		break;

	case CATIAPluginCaller_MODEL_IS_LOADED:
		value = isModelLoaded (env, iObjectPtr);
		break;
		
	default:
		CATIA_DEBUG ("callBooleanFunctions: Undefined function index ("
					<< iFunctionIndex << ")" << endl);
	}

	return value;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a double
/////////////////////////////////////////////////////////////////////
double CATIADelegate::callDoubleFunctions (JNIEnv* env, jobjectArray args, 
										   unsigned int iNumArgs,
										   long iObjectPtr,
										   unsigned int iFunctionIndex)
{
	double value = 0;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	case CATIAPluginCaller_REAL_GETVALUE:
	case CATIAPluginCaller_LENGTH_GETVALUE:
	case CATIAPluginCaller_ANGLE_GETVALUE:
	case CATIAPluginCaller_MASS_GETVALUE:
	case CATIAPluginCaller_VOLUME_GETVALUE:
	case CATIAPluginCaller_DENSITY_GETVALUE:
	case CATIAPluginCaller_AREA_GETVALUE:
		value = getDoubleValue (env, iObjectPtr);
		break;

	default:
		CATIA_DEBUG ("callDoubleFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return value;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a character string
/////////////////////////////////////////////////////////////////////
const
char* CATIADelegate::callStringFunctions (JNIEnv* env, jobjectArray args, 
										  unsigned int iNumArgs,
										  long iObjectPtr,
										  unsigned int iFunctionIndex)
{
	const char *value = NULL;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	case CATIAPluginCaller_STRING_GETVALUE:
		value = getStringValue (env, iObjectPtr);
		break;

	default:
		CATIA_DEBUG ("callStringFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return value;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a 1D array of doubles
/////////////////////////////////////////////////////////////////////
vector<double>
CATIADelegate::callDoubleArrayFunctions (JNIEnv* env, jobjectArray args,
										 unsigned int iNumArgs,
										 long iObjectPtr,
										 unsigned int iFunctionIndex)
{
	vector<double> result;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	case CATIAPluginCaller_VECTOR_GETVALUE:
		result = getDoubleArrayValue (env, iObjectPtr);
		break;

	default:
		CATIA_DEBUG ("callDoubleArrayFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return result;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a 1D array of integers
/////////////////////////////////////////////////////////////////////
vector<int>
CATIADelegate::callIntegerArrayFunctions (JNIEnv* env, jobjectArray args,
										  unsigned int iNumArgs,
										  long iObjectPtr,
										  unsigned int iFunctionIndex)
{
	vector<int> result;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	default:
		CATIA_DEBUG ("callIntegerArrayFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return result;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a 2D array of integers
/////////////////////////////////////////////////////////////////////
vector< vector<int> >
CATIADelegate::call2DIntegerArrayFunctions (JNIEnv* env, jobjectArray args,
											unsigned int iNumArgs,
											long iObjectPtr,
											unsigned int iFunctionIndex)
{
	vector < vector<int> > result;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	default:
		CATIA_DEBUG ("call2DIntegerArrayFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return result;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a 2D array of doubles
/////////////////////////////////////////////////////////////////////
vector< vector<double> > 
CATIADelegate::call2DDoubleArrayFunctions (JNIEnv* env, jobjectArray args,
										   unsigned int iNumArgs,
										   long iObjectPtr,
										   unsigned int iFunctionIndex)
{
	vector< vector<double> > result;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	default:
		CATIA_DEBUG ("call2DDoubleArrayFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return result;
}



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// void functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


/////////////////////////////////////////////////////
// load the model
/////////////////////////////////////////////////////
void CATIADelegate::loadModel (JNIEnv* env, long iObjectPtr)
{
	CATIAModel *model = (CATIAModel*) iObjectPtr;

	if (model == NULL) {
		CATIA_DEBUG ("loadModel: Model does not exist" << endl);
	}
	else
	{
		model->loadModel ();
	}
}


/////////////////////////////////////////////////////
// unload the model
/////////////////////////////////////////////////////
void CATIADelegate::unloadModel( JNIEnv* env, jobjectArray args,
									  unsigned int iNumArgs, long iObjectPtr)
{
	// check argument list length
	if (iNumArgs != 1) {
		CATIA_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the unloadModel function");
	}
	else 
	{
		// unpack the arguments
		int err2;
		CATIAModel *model = (CATIAModel*) iObjectPtr;
		void* iNumModels = NULL;
		err2 = getArgument (env, args, 0, JAVA_INTEGER, iNumModels);

		if (model == NULL) {
			CATIA_DEBUG ("unloadModel: Model does not exist" << endl);
		}
		else
		if (err2 != ERR_OK) {
			CATIA_DEBUG ("unloadModel: Invalid number of arguments (ERR = "
						 << err2 << ")" << endl);
		}
		else
		{
			// make the call
			model->unloadModel (*((int*)iNumModels));
		}
	}
}


/////////////////////////////////////////////////////
// destroy the model
/////////////////////////////////////////////////////
void CATIADelegate::destroyModel (JNIEnv* env, long iObjectPtr)
{
	CATIAModel *model = (CATIAModel*) iObjectPtr;

	if (model == NULL) {
		CATIA_DEBUG ("destroyModel: Model does not exist" << endl);
	}
	else
	{
		delete model;
	}
}


/////////////////////////////////////////////////////
// create a user library
/////////////////////////////////////////////////////
void CATIADelegate::setupUserLibrary (JNIEnv* env, jobjectArray args,
									  unsigned int iNumArgs, long iObjectPtr)
{
	// check argument list length
	if (iNumArgs != 2) {
		CATIA_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the setupUserLibrary function");
	}
	else 
	{
		// unpack the arguments
		int err1, err2;
		CATIAModel *model = (CATIAModel*) iObjectPtr;
		void* szParameterName = NULL;
		void* iNumArguments = NULL;
		err1 = getArgument (env, args, 0, JAVA_STRING, szParameterName);
		err2 = getArgument (env, args, 1, JAVA_INTEGER, iNumArguments);

		if (model == NULL) {
			CATIA_DEBUG ("setupUserLibrary: Model does not exist" << endl);
		}
		else
		if (err1 != ERR_OK) {
			CATIA_DEBUG ("setupUserLibrary: Invalid parameter name (ERR = "
						 << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			CATIA_DEBUG ("setupUserLibrary: Invalid number of arguments (ERR = "
						 << err2 << ")" << endl);
		}
		else
		{
			// make the call
			model->setupUserLibrary ((char*)szParameterName, *((int*)iNumArguments));
		}

		// clean up
		if (szParameterName != NULL)
			delete szParameterName;
		if (iNumArguments != NULL)
			delete iNumArguments;
	}
}


/////////////////////////////////////////////////////
// set values
/////////////////////////////////////////////////////
void CATIADelegate::setValue (JNIEnv* env, jobjectArray args,
							  unsigned int iNumArgs,
							  long iObjectPtr,
							  unsigned int iFunctionIndex)
{
	// check argument list length
	if (iNumArgs != 1) {
		CATIA_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the setValue function");
	}
	else 
	{
		CATIAData *parameter = (CATIAData*) iObjectPtr;
		if (parameter == NULL) {
			CATIA_DEBUG ("setValue: Parameter does not exist" << endl);
		}
		else
		{
			int err;
			void *value = NULL;

			// unpack the argument and call the function
			switch (iFunctionIndex)
			{
			case CATIAPluginCaller_REAL_SETVALUE:
			case CATIAPluginCaller_LENGTH_SETVALUE:
			case CATIAPluginCaller_ANGLE_SETVALUE:
			case CATIAPluginCaller_MASS_SETVALUE:
			case CATIAPluginCaller_VOLUME_SETVALUE:
			case CATIAPluginCaller_DENSITY_SETVALUE:
			case CATIAPluginCaller_AREA_SETVALUE:
				err = getArgument (env, args, 0, JAVA_DOUBLE, value);
				if (err != ERR_OK) {
					CATIA_DEBUG ("setValue: Invalid parameter value (ERR = "
								 << err << ")" << endl);
				}
				else {
					((CATIAReal*)parameter)->setValue ( *((double*)value) );
				}
				break;

			case CATIAPluginCaller_INT_SETVALUE:
				err = getArgument (env, args, 0, JAVA_INTEGER, value);
				if (err != ERR_OK) {
					CATIA_DEBUG ("setValue: Invalid parameter value (ERR = "
								 << err << ")" << endl);
				}
				else {
					((CATIAInteger*)parameter)->setValue ( *((int*)value) );
				}
				break;

			case CATIAPluginCaller_BOOL_SETVALUE:
				err = getArgument (env, args, 0, JAVA_BOOLEAN, value);
				if (err != ERR_OK) {
					CATIA_DEBUG ("setValue: Invalid parameter value (ERR = "
								 << err << ")" << endl);
				}
				else {
					((CATIABoolean*)parameter)->setValue ( *((bool*)value) );
				}
				break;

			case CATIAPluginCaller_STRING_SETVALUE:
				err = getArgument (env, args, 0, JAVA_STRING, value);
				if (err != ERR_OK) {
					CATIA_DEBUG ("setValue: Invalid parameter value (ERR = "
								 << err << ")" << endl);
				}
				else {
					((CATIAString*)parameter)->setValue ( (char*)value );
				}
				break;

			default:
				CATIA_DEBUG ("setValue: Undefined function index ("
							 << iFunctionIndex << ")" << endl);
			}

			// clean up
			if (value != NULL)
				delete value;
		}
	}
}


/////////////////////////////////////////////////////////////////////
// set the values for a 1D array
/////////////////////////////////////////////////////////////////////
void CATIADelegate::set1DArrayValues (JNIEnv* env, jobjectArray args,
									  unsigned int iNumArgs,
									  long iObjectPtr,
									  unsigned int iFunctionIndex)
{
	// check argument list length
	if (iNumArgs != 2) {
		CATIA_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the set1DArrayValues function" << endl);
	}
	else 
	{
		int err1, err2;
		void *iNumElements = NULL;
		void *dValues = NULL;

		CATIAData *parameter = (CATIAData*) iObjectPtr;
		err1 = getArgument (env, args, 0, JAVA_INTEGER, iNumElements);
		err2 = getArgument (env, args, 1, JAVA_1D_DOUBLE_ARRAY, dValues);

		if (parameter == NULL) {
			CATIA_DEBUG ("setValue: Parameter does not exist" << endl);
		}
		else
		if (err1 != ERR_OK) {
			CATIA_DEBUG ("setValue: Invalid number of elements "
						 << "(ERR = " << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			CATIA_DEBUG ("setValue: Invalid array values "
						 << "(ERR = " << err2 << ")" << endl);
		}
		else
		{
			((CATIAVector*)parameter)->setValue ( *((int*)iNumElements),
												  (double*)dValues );
		}

		// clean up
		if (iNumElements != NULL)
			delete iNumElements;
		if (dValues != NULL)
			delete dValues;
	}
}



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// object functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


///////////////////////////////////////////
// create the model
///////////////////////////////////////////
CATIAModel *CATIADelegate::initializeModel (JNIEnv* env, jobjectArray args,
											unsigned int iNumArgs)
{
	CATIAModel *model = NULL;

	CATIA_DEBUG("Start to initialize model!");
	// check argument list length
	if (iNumArgs != 2) {
		CATIA_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the initializeModel function" << endl);
	}
	else 
	{
		int err1, err2;
		void* szFileName = NULL;
		void* bIsVisible = NULL;

		// unpack the arguments
		err1 = getArgument (env, args, 0, JAVA_STRING, szFileName);
		err2 = getArgument (env, args, 1, JAVA_BOOLEAN, bIsVisible);

		if (err1 != ERR_OK) {
			CATIA_DEBUG ("initializeModel: Invalid model file name (ERR = "
						 << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			CATIA_DEBUG ("initializeModel: Invalid argument: bIsVisible (ERR = " 
						 << err2 << ")" << endl);
		}
		else {
			// call the constructor
			model = new CATIAModel ((char*)szFileName, *((bool*)bIsVisible) );
		}

		// clean up
		if (szFileName != NULL)
			delete szFileName;
		if (bIsVisible != NULL)
			delete bIsVisible;
	}

	return model;
}


///////////////////////////////////////////
// create individual model objects
///////////////////////////////////////////
CATIAData *CATIADelegate::createModelObject (JNIEnv* env, jobjectArray args,
											 unsigned int iNumArgs,
											 long iObjectPtr,
											 unsigned int iFunctionIndex)
{
	CATIAData *modelObject = NULL;

	// check argument list length
	if (iNumArgs != 1) {
		CATIA_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the createModelObject function" << endl);
	}
	else 
	{
		int err;
		void* szParameterName = NULL;

		// unpack the arguments
		CATIAModel *model = (CATIAModel*) iObjectPtr;
		err = getArgument (env, args, 0, JAVA_STRING, szParameterName);

		if (model == NULL) {
			CATIA_DEBUG ("createModelObject: Model does not exist" << endl);
		}
		else
		if (err != ERR_OK) {
			CATIA_DEBUG ("createModelObject: Invalid parameter name (ERR = "
						 << err << ")" << endl);
		}
		else
		{
			// call the function
			switch (iFunctionIndex)
			{
			case CATIAPluginCaller_MODEL_CREATE_REAL:
				modelObject = model->createReal ((char*)szParameterName);
				break;
			case CATIAPluginCaller_MODEL_CREATE_INT:
				modelObject = model->createInteger ((char*)szParameterName);
				break;
			case CATIAPluginCaller_MODEL_CREATE_STRING:
				modelObject = model->createString ((char*)szParameterName);
				break;
			case CATIAPluginCaller_MODEL_CREATE_BOOL:
				modelObject = model->createBoolean ((char*)szParameterName);
				break;
			case CATIAPluginCaller_MODEL_CREATE_LENGTH:
				modelObject = model->createReal ((char*)szParameterName);
				break;
			case CATIAPluginCaller_MODEL_CREATE_ANGLE:
				modelObject = model->createReal ((char*)szParameterName);
				break;
			case CATIAPluginCaller_MODEL_CREATE_MASS:
				modelObject = model->createReal ((char*)szParameterName);
				break;
			case CATIAPluginCaller_MODEL_CREATE_VOLUME:
				modelObject = model->createReal ((char*)szParameterName);
				break;
			case CATIAPluginCaller_MODEL_CREATE_DENSITY:
				modelObject = model->createReal ((char*)szParameterName);
				break;
			case CATIAPluginCaller_MODEL_CREATE_AREA:
				modelObject = model->createReal ((char*)szParameterName);
				break;

			default:
				CATIA_DEBUG ("createModelObject: Undefined function index ("
							 << iFunctionIndex << ")" << endl);
			}
		}

		// clean up
		if (szParameterName != NULL)
			delete szParameterName;
	}

	return modelObject;
}


///////////////////////////////////////////
// create file object
///////////////////////////////////////////
CATIAFile *CATIADelegate::createFileObject (JNIEnv* env, jobjectArray args,
											unsigned int iNumArgs,
											long iObjectPtr)
{
	CATIAFile *modelObject = NULL;

	// check argument list length
	if (iNumArgs != 2) {
		CATIA_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the createFileObject function" << endl);
	}
	else 
	{
		int err1, err2;
		void* szParameterName = NULL;
		void* szFileType = NULL;

		// unpack the arguments
		CATIAModel *model = (CATIAModel*) iObjectPtr;
		err1 = getArgument (env, args, 0, JAVA_STRING, szParameterName);
		err2 = getArgument (env, args, 1, JAVA_STRING, szFileType);

		if (model == NULL) {
			CATIA_DEBUG ("createFileObject: Model does not exist" << endl);
		}
		else
		if (err1 != ERR_OK) {
			CATIA_DEBUG ("createFileObject: Invalid parameter name (ERR = "
						 << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			CATIA_DEBUG ("createFileObject: Invalid file type (ERR = "
						 << err2 << ")" << endl);
		}
		else
		{
			// call the function
			modelObject = model->createFile ((char*)szParameterName, (char*)szFileType);
		}

		// clean up
		if (szParameterName != NULL)
			delete szParameterName;
		if (szFileType != NULL)
			delete szFileType;
	}

	return modelObject;
}


///////////////////////////////////////////////
// create a string object for a user library
///////////////////////////////////////////////
CATIAData *CATIADelegate::createUserLibraryModelObject (JNIEnv* env, jobjectArray args, 
														unsigned int iNumArgs, 
														long iObjectPtr,
														unsigned int iFunctionIndex)
{
	CATIAData *modelObject = NULL;

	// check argument list length
	if (iNumArgs != 3) {
		CATIA_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the createUserLibraryModelObject function" << endl);
	}
	else 
	{
		int err1, err2, err3;
		void* szParameterName = NULL;
		void* szLibraryName = NULL;
		void* iArgIndex = NULL;

		// unpack the arguments
		CATIAModel *model = (CATIAModel*) iObjectPtr;
		err1 = getArgument (env, args, 0, JAVA_STRING, szParameterName);
		err2 = getArgument (env, args, 1, JAVA_STRING, szLibraryName);
		err3 = getArgument (env, args, 2, JAVA_INTEGER, iArgIndex);

		if (model == NULL) {
			CATIA_DEBUG ("createUserLibraryModelObject: Model does not exist" << endl);
		}
		else
		if (err1 != ERR_OK) {
			CATIA_DEBUG ("createUserLibraryModelObject: Invalid parameter name (ERR = "
						 << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			CATIA_DEBUG ("createUserLibraryModelObject: Invalid library name for vector (ERR = "
						 << err2 << ")" << endl);
		}
		else
		if (err3 != ERR_OK) {
			CATIA_DEBUG ("createUserLibraryModelObject: Invalid argument index for vector (ERR = "
						 << err3 << ")" << endl);
		}
		else
		{
			// call the function
			switch (iFunctionIndex)
			{
			case CATIAPluginCaller_MODEL_CREATE_STRING_USERLIB:
				modelObject = model->createStringUserLib (
											(char*)szParameterName,
											(char*)szLibraryName, 
											*((int*)iArgIndex) );
				break;

			case CATIAPluginCaller_MODEL_CREATE_REAL_USERLIB:
				modelObject = model->createRealUserLib (
											(char*)szParameterName,
											(char*)szLibraryName, 
											*((int*)iArgIndex) );
				break;

			case CATIAPluginCaller_MODEL_CREATE_INTEGER_USERLIB:
				modelObject = model->createIntegerUserLib (
											(char*)szParameterName,
											(char*)szLibraryName, 
											*((int*)iArgIndex) );
				break;

			case CATIAPluginCaller_MODEL_CREATE_BOOLEAN_USERLIB:
				modelObject = model->createBooleanUserLib (
											(char*)szParameterName,
											(char*)szLibraryName, 
											*((int*)iArgIndex) );
				break;

			default:
				CATIA_DEBUG ("createUserLibraryModelObject: Undefined function index "
							 << "(" << iFunctionIndex << ")" << endl);
			}
		}

		// clean up
		if (szParameterName != NULL)
			delete szParameterName;
		if (szLibraryName != NULL)
			delete szLibraryName;
		if (iArgIndex != NULL)
			delete iArgIndex;
	}

	return modelObject;
}


/////////////////////////////////////////////////////
// create a vector object for a user library
/////////////////////////////////////////////////////
CATIAVector *CATIADelegate::createUserLibraryVectorObject (JNIEnv* env, jobjectArray args, 
														   unsigned int iNumArgs, 
														   long iObjectPtr)
{
	CATIAVector *modelObject = NULL;

	// check argument list length
	if (iNumArgs != 3) {
		CATIA_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the createUserLibraryVectorObject function");
	}
	else 
	{
		int err1, err2, err3;
		void* szParameterName = NULL;
		void* iNumElements = NULL;
		void* szLibraryName = NULL;
		void* iArgIndex = NULL;

		// unpack the arguments
		CATIAModel *model = (CATIAModel*) iObjectPtr;
		err1 = getArgument (env, args, 0, JAVA_STRING, szParameterName);
		err2 = getArgument (env, args, 1, JAVA_STRING, szLibraryName);
		err3 = getArgument (env, args, 2, JAVA_INTEGER, iArgIndex);

		if (model == NULL) {
			CATIA_DEBUG ("createModelObject: Model does not exist" << endl);
		}
		else
		if (err1 != ERR_OK) {
			CATIA_DEBUG ("createUserLibraryVectorObject: Invalid parameter name (ERR = "
						 << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			CATIA_DEBUG ("createUserLibraryVectorObject: Invalid library name (ERR = "
						 << err2 << ")" << endl);
		}
		else
		if (err3 != ERR_OK) {
			CATIA_DEBUG ("createUserLibraryVectorObject: Invalid argument index (ERR = "
						 << err3 << ")" << endl);
		}
		else
		{
			// call the function
			modelObject = model->createVectorUserLib (	(char*)szParameterName,
														(char*)szLibraryName, 
														*((int *)iArgIndex) );
		}

		// clean up
		if (szParameterName != NULL)
			delete szParameterName;
		if (iNumElements != NULL)
			delete iNumElements;
		if (szLibraryName != NULL)
			delete szLibraryName;
		if (iArgIndex != NULL)
			delete iArgIndex;
	}

	return modelObject;
}


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// real functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


///////////////////////////////////////////
// get the value from a real parameter
///////////////////////////////////////////
double CATIADelegate::getDoubleValue (JNIEnv* env, long iObjectPtr)
{
	double value = 0;
	CATIAReal *real = (CATIAReal*) iObjectPtr;

	if (real == NULL) {
		CATIA_DEBUG ("getDoubleValue: CATIAReal does not exist" << endl);
	}
	else {
		// get the value
		value = real->getValue ();		
	}

	return value;
}


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// integer functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


///////////////////////////////////////////
// get the value from an integer parameter
///////////////////////////////////////////
int CATIADelegate::getIntegerValue (JNIEnv* env, long iObjectPtr)
{
	int value = 0;
	CATIAInteger *integer = (CATIAInteger*) iObjectPtr;

	if (integer == NULL) {
		CATIA_DEBUG ("getIntegerValue: CATIAInteger does not exist" << endl);
	}
	else {
		// get the value
		value = integer->getValue ();
	}

	return value;
}


///////////////////////////////////////////
// execute the model
///////////////////////////////////////////
int CATIADelegate::executeModel (JNIEnv* env, long iObjectPtr)
{
	int value = 0;
	CATIAModel *model = (CATIAModel*) iObjectPtr;

	if (model == NULL) {
		CATIA_DEBUG ("executeModel: CATIAModel does not exist" << endl);
	}
	else {
		// get the value
		value = model->execute ();
	}

	return value;
}



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// boolean functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


///////////////////////////////////////////
// get the value from a real parameter
///////////////////////////////////////////
bool CATIADelegate::getBooleanValue (JNIEnv* env, long iObjectPtr)
{
	bool value = false;
	CATIABoolean *boolean = (CATIABoolean*) iObjectPtr;

	if (boolean == NULL) {
		CATIA_DEBUG ("getBooleanValue: CATIABoolean does not exist" << endl);
	}
	else {
		// get the value
		value = boolean->getValue ();
	}

	return value;
}

///////////////////////////////////////////
// check if model is loaded
///////////////////////////////////////////
bool CATIADelegate::isModelLoaded (JNIEnv* env, long iObjectPtr)
{
	bool value = false;
	CATIAModel *model = (CATIAModel*) iObjectPtr;

	if (model == NULL) {
		CATIA_DEBUG ("isModelLoaded: Model does not exist" << endl);
	}
	else {
		// get the value
		value = model->isModelLoaded ();
	}

	return value;
}


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// string functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


///////////////////////////////////////////
// get the value from a string parameter
///////////////////////////////////////////
const char* CATIADelegate::getStringValue (JNIEnv* env, long iObjectPtr)
{
	const char* value = NULL;
	CATIAString *str = (CATIAString*) iObjectPtr;

	if (str == NULL) {
		CATIA_DEBUG ("getStringValue: CATIAString does not exist" << endl);
	}
	else {
		// get the value
		value = str->getValue ();
	}

	return value;
}



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// array functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


/////////////////////////////////////////////////
// get the value from a double array parameter
/////////////////////////////////////////////////
vector<double> CATIADelegate::getDoubleArrayValue (JNIEnv* env, long iObjectPtr)
{
	vector<double> value;
	CATIAVector *vectorObj = (CATIAVector*)iObjectPtr;

	if (vectorObj == NULL) {
		CATIA_DEBUG ("getDoubleArrayValue: CATIAVector does not exist" << endl);
	}
	else {
		value =  vectorObj->getValue ();
	}

	return value;
}

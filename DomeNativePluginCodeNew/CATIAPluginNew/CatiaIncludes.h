// CATIAIncludes.h: interface for the CATIAIncludes class.
//
//////////////////////////////////////////////////////////////////////

#ifndef CATIA_INCLUDES_H
#define CATIA_INCLUDES_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include <iostream.h>

// System includes
#include "CATLib.h"

// Automation includes
#include "CATIAApplication.h"
#include "CATIADocuments.h"
#include "CATIADocument.h"
#include "CATIAPartDocument.h"
#include "CATIAPart.h"
#include "CATIABodies.h"
#include "CATIAParameters.h"
#include "CATIAParameter.h"

#include "CATIPartRequest.h"
#include "CATIEditor.h"
#include "CATIADomeInterface.h"

#include "CATSafeArray.h"
#include "CATAutoConversions.h"

// ObjectModelerBase includes
#include "CATDocument.h"
#include "CATSessionServices.h" 
#include "CATDocumentServices.h" 
#include "CATInit.h"
#include "CATIContainer.h"
#include "CATIDescendants.h"

// ObjectSpecsModeler includes
#include "CATISpecObject.h"
#include "CATLISTV_CATISpecObject.h"
#include "CATLISTV_CATISpecAttribute.h"


// NewTopologicalObjects includes
#include "CATBody.h"
#include "CATGeometry.h"
#include "CATCell.h"

// MechanicalModeler includes
#include "CATIPrtContainer.h"
#include "CATIMfGeometryAccess.h"
#include "CATIBRepAccess.h"
#include "CATMfBRepDecode.h"
#include "CATIPrtPart.h"
#include "CATIMechanicalPartProperties.h"

// MechanicalModelerUI Framework
#include "CATPrtUpdateCom.h"      // needed to update the feature according to the user's update settings


// Visualization includes
#include "CATIVisProperties.h"
#include "CATVisPropertiesValues.h"

// Literal Features
#include "CATICkeParm.h"
#include "CATIParmPublisher.h"

#include "CATIPad.h"
#include "CATIPrism.h"

#include "CATISpecAttrAccess.h"

//For Moments of Intertia
#include "CATCollec.h"

// Visualization Framework
#include "CATVisManager.h"           // The object to create the graphic representation (rep)          
#include "CATI3DGeoVisu.h"           // The visualization interface for the reps 
#include "CATPathElement.h"          // To create a path with the object to compute the rep
#include "CAT3DViewpoint.h"          // To create a default viewpoint

// error handling
#include "CATError.h"
#include "CATMfErrUpdate.h"   

#define CATIA_ERROR(msg)	cout<<"CATIA Error: "<<msg<<endl;
#define CATIA_DEBUG(msg)	cout<<"CATIA Debug: "<<msg<<endl;
#define DOME_NOT_SUPPORTED(msg)	cout <<"DOME NOT SUPPORTED" << msg << endl;


#endif // CATIA_INCLUDES_H

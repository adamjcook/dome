#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"
#include <windows.h>
#include <iostream>   // std::cout, std::endl
#include <vector.h>
using namespace std;


//------------------------------------------------------------------
// Create the delegate object
//
// *********** PLUGIN AUTHOR MODIFIES THIS SECTION ONLY ************
//
#include "CATIAPluginCaller.h"
#include "CATIADelegate.h"
CATIADelegate delegate;
//------------------------------------------------------------------


//------------------------------------------------------------------
// The following parameters were the start of an attempt to
// create a GUI debug window that can be used to display messages 
// while the plugin is executing. The attempt was not immediately
// and was abandoned for other priorities. The variables and the
// functions that use them were left here in case anyone wants
// to give the debug window another go.
HWND hedit;
HINSTANCE hinst;
HANDLE guiThread;
//------------------------------------------------------------------


//------------------------------------------------------------------
// function prototypes
jstring JNU_NewStringNative(JNIEnv *env, const char *str);
//------------------------------------------------------------------



/*
 * Class:     mit_cadlab_dome3_plugin_CATIAPluginCaller
 * Method:    callVoidFunc
 */
JNIEXPORT void JNICALL Java_mit_cadlab_dome3_plugin_catia_CATIAPluginCaller_callVoidFunc
  (JNIEnv *env, jobject thisobj, jlong iObjectPtr, jint iFunction, jobjectArray args)
{
	int iNumArgs = 0;
	if (args != NULL) {
		iNumArgs = env->GetArrayLength(args);
	}
	delegate.callVoidFunctions (env, args, iNumArgs, iObjectPtr, iFunction);
}


/*
 * Class:     mit_cadlab_dome3_plugin_CATIAPluginCaller
 * Method:    callBoolFunc
 */
JNIEXPORT jboolean JNICALL Java_mit_cadlab_dome3_plugin_catia_CATIAPluginCaller_callBoolFunc
  (JNIEnv *env, jobject thisobj, jlong iObjectPtr, jint iFunction, jobjectArray args)
{
	jboolean value;
	int iNumArgs = 0;
	if (args != NULL) {
		iNumArgs = env->GetArrayLength(args);
	}
	value = delegate.callBooleanFunctions (env, args, iNumArgs, iObjectPtr, iFunction);

	return value;
}


/*
 * Class:     mit_cadlab_dome3_plugin_CATIAPluginCaller
 * Method:    callIntFunc
 */
JNIEXPORT jint JNICALL Java_mit_cadlab_dome3_plugin_catia_CATIAPluginCaller_callIntFunc
  (JNIEnv *env, jobject thisobj, jlong iObjectPtr, jint iFunction, jobjectArray args)
{
	jint value;
	int iNumArgs = 0;
	if (args != NULL) {
		iNumArgs = env->GetArrayLength(args);
	}
	value = delegate.callIntegerFunctions (env, args, iNumArgs, iObjectPtr, iFunction);

	return value;
}


/*
 * Class:     mit_cadlab_dome3_plugin_CATIAPluginCaller
 * Method:    callDoubleFunc
 */
JNIEXPORT jdouble JNICALL Java_mit_cadlab_dome3_plugin_catia_CATIAPluginCaller_callDoubleFunc
  (JNIEnv *env, jobject thisobj, jlong iObjectPtr, jint iFunction, jobjectArray args)
{
	jdouble value;
	int iNumArgs = 0;
	if (args != NULL) {
		iNumArgs = env->GetArrayLength(args);
	}
	value = delegate.callDoubleFunctions (env, args, iNumArgs, iObjectPtr, iFunction);

	return value;
}


/*
 * Class:     mit_cadlab_dome3_plugin_CATIAPluginCaller
 * Method:    callStringFunc
 */
JNIEXPORT jstring JNICALL Java_mit_cadlab_dome3_plugin_catia_CATIAPluginCaller_callStringFunc
  (JNIEnv *env, jobject thisobj, jlong iObjectPtr, jint iFunction, jobjectArray args)
{
	const char *value;
	int iNumArgs = 0;
	if (args != NULL) {
		iNumArgs = env->GetArrayLength(args);
	}
	value = delegate.callStringFunctions (env, args, iNumArgs, iObjectPtr, iFunction);

	jstring returnStr = JNU_NewStringNative(env, value);
	return returnStr;
}


/*
 * Class:     mit_cadlab_dome3_plugin_CATIAPluginCaller
 * Method:    callObjectFunc
 */
JNIEXPORT jlong JNICALL Java_mit_cadlab_dome3_plugin_catia_CATIAPluginCaller_callObjectFunc
  (JNIEnv *env, jobject thisobj, jlong iObjectPtr, jint iFunction, jobjectArray args)
{
	int iNumArgs = 0;
	if (args != NULL) {
		iNumArgs = env->GetArrayLength(args);
	}
	iObjectPtr = delegate.callObjectFunctions (env, args, iNumArgs, iObjectPtr, iFunction);

	return iObjectPtr;
}



/*
 * Class:     mit_cadlab_dome3_plugin_CATIAPluginCaller
 * Method:    callDoubleArrayFunc
 */
JNIEXPORT jdoubleArray JNICALL Java_mit_cadlab_dome3_plugin_catia_CATIAPluginCaller_callDoubleArrayFunc
  (JNIEnv *env, jobject thisobj, jlong iObjectPtr, jint iFunction, jobjectArray args)
{
	int iNumArgs = 0;
	if (args != NULL) {
		iNumArgs = env->GetArrayLength(args);
	}
	
	vector<double> array =
		delegate.callDoubleArrayFunctions (env, args, iNumArgs, iObjectPtr, iFunction);

	int arrlength = array.size();
	jdoubleArray darr = env->NewDoubleArray(arrlength);
	if(darr == 0) {
		return 0; //out of memory
	}
	double* arr = new double[arrlength];
	for(int i = 0; i < arrlength; i++) {
		arr[i] = array[i];
	}
	env->SetDoubleArrayRegion(darr, 0, arrlength, arr);
	delete arr;

	return darr;
}


/*
 * Class:     mit_cadlab_dome3_plugin_CATIAPluginCaller
 * Method:    callIntArrayFunc
 */
JNIEXPORT jintArray JNICALL Java_mit_cadlab_dome3_plugin_catia_CATIAPluginCaller_callIntArrayFunc
  (JNIEnv *env, jobject thisobj, jlong iObjectPtr, jint iFunction, jobjectArray args)
{
	int iNumArgs = 0;
	if (args != NULL) {
		iNumArgs = env->GetArrayLength(args);
	}
	vector<int> array =
		delegate.callIntegerArrayFunctions (env, args, iNumArgs, iObjectPtr, iFunction);

	int arrlength = array.size();
	jintArray iarr = env->NewIntArray(arrlength);
	if(iarr == 0) {
		return 0; //out of memory
	}
	long* arr = new long[arrlength];
	for(int i = 0; i < arrlength; i++) {
		arr[i] = array[i];
	}
	env->SetIntArrayRegion(iarr, 0, arrlength, arr);
	delete arr;
	return iarr;
}


/*
 * Class:     mit_cadlab_dome3_plugin_CATIAPluginCaller
 * Method:    call2DimDoubleArrayFunc
 */
JNIEXPORT jobjectArray JNICALL Java_mit_cadlab_dome3_plugin_catia_CATIAPluginCaller_call2DimDoubleArrayFunc
  (JNIEnv *env, jobject thisobj, jlong iObjectPtr, jint iFunction, jobjectArray args)
{
	int iNumArgs = 0;
	if (args != NULL) {
		iNumArgs = env->GetArrayLength(args);
	}
	vector< vector<double> > array
		= delegate.call2DDoubleArrayFunctions (env, args, iNumArgs, iObjectPtr, iFunction);

	int rows = array.size();
	jclass dubArrCls =  env->FindClass("[D");
	if (dubArrCls == 0) {
		return 0;
	}
	jobjectArray objarr = env->NewObjectArray(rows, dubArrCls, 0);
	for(int i = 0; i < rows; i++) {
		vector<double> vec = array[i];
		int cols = vec.size();
		double* arr = new double[cols];
		jdoubleArray darr = env->NewDoubleArray(cols);
		if(darr == 0) {
		return 0; //out of memory
		}
		for(int j = 0; j < cols; j++) {
			arr[j] = vec[j];
		}
		env->SetDoubleArrayRegion(darr, 0, cols, arr);
		env->SetObjectArrayElement(objarr, i, darr);
        env->DeleteLocalRef(darr);
		delete arr;
	}
	return objarr;
}


/*
 * Class:     mit_cadlab_dome3_plugin_CATIAPluginCaller
 * Method:    call2DimIntArrayFunc
 */
JNIEXPORT jobjectArray JNICALL Java_mit_cadlab_dome3_plugin_catia_CATIAPluginCaller_call2DimIntArrayFunc
  (JNIEnv *env, jobject thisobj, jlong iObjectPtr, jint iFunction, jobjectArray args)
{
	int iNumArgs = 0;
	if (args != NULL) {
		iNumArgs = env->GetArrayLength(args);
	}
	vector< vector<int> > array
		= delegate.call2DIntegerArrayFunctions (env, args, iNumArgs, iObjectPtr, iFunction);

	int rows = array.size();
	jclass intArrCls =  env->FindClass("[I");
	if (intArrCls == 0) {
		return 0;
	}
	jobjectArray objarr = env->NewObjectArray(rows, intArrCls, 0);
	for(int i = 0; i < rows; i++) {
		vector<int> vec = array[i];
		int cols = vec.size();
		long* arr = new long[cols];
		jintArray darr = env->NewIntArray(cols);
		if(darr == 0) {
		return 0; //out of memory
		}
		for(int j = 0; j < cols; j++) {
			arr[j] = vec[j];
		}
		env->SetIntArrayRegion(darr, 0, cols, arr);
		env->SetObjectArrayElement(objarr, i, darr);
        env->DeleteLocalRef(darr);
		delete arr;
	}
	return objarr;
}



jstring JNU_NewStringNative(JNIEnv *env, const char *str)
{
	jstring result;
	jbyteArray bytes = 0;
	int len;
	if (env->EnsureLocalCapacity(2) < 0) {
		return 0; /* out of memory error */
	}
	len = strlen(str);
	bytes = env->NewByteArray(len);
	if (bytes != 0) {
		env->SetByteArrayRegion(bytes, 0, len,
			(jbyte *)str);
		jclass cls =  env->FindClass("java/lang/String");
		if (cls == 0) {
			return 0;
		}
		jmethodID mid = env->GetMethodID(cls, "<init>", "([B)V");
		if (mid == 0) {
			return 0;
		}
		result = (jstring)env->NewObject(cls, mid, bytes);
		env->DeleteLocalRef(bytes);
		return result;
	} /* else fall through */
	return 0;
}


int ShowError ()
{
	LPVOID lpMsgBuf;
	if (!FormatMessage( 
		FORMAT_MESSAGE_ALLOCATE_BUFFER | 
		FORMAT_MESSAGE_FROM_SYSTEM | 
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		GetLastError(),
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
		(LPTSTR) &lpMsgBuf,
		0,
		NULL ))
	{
	   // Handle the error.
	   return -1;
	}

	// Process any inserts in lpMsgBuf.
	// ...

	// Display the string.
	MessageBox( NULL, (LPCTSTR)lpMsgBuf, "Error", MB_OK | MB_ICONERROR);

	// Free the buffer.
	LocalFree( lpMsgBuf );

	return 0;
}


BOOL CALLBACK GoToProc(HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam) 
{ 
	switch (message) 
    { 
        case WM_INITDIALOG: 
            return TRUE; 
 
        case WM_COMMAND: 
            switch (LOWORD(wParam)) 
            { 
                case IDOK: 
                case IDCANCEL: 
                    return TRUE; 
            } 
    } 

    return FALSE;
}


DWORD WINAPI ThreadProc(LPVOID lpParameter)
{
	hinst = GetModuleHandle(NULL);

	/*
	// Fill in the window class structure with parameters 
    // that describe the main window. 
 	WNDCLASSEX wcx; 
    wcx.cbSize = sizeof(wcx);									// size of structure 
    wcx.style = CS_HREDRAW | CS_VREDRAW;						// redraw if size changes 
    wcx.lpfnWndProc = MainWndProc;								// points to window procedure 
    wcx.cbClsExtra = 0;											// no extra class memory 
    wcx.cbWndExtra = 0;											// no extra window memory 
    wcx.hInstance = hinst;										// handle to instance 
	wcx.hIcon = NULL;
    wcx.hCursor = LoadCursor(NULL, IDC_ARROW);                  // predefined arrow 
    wcx.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);    // white background brush 
	wcx.lpszMenuName = NULL;
    wcx.lpszClassName = "MainWClass";							// name of window class 
    wcx.hIconSm = (HICON)LoadImage(hinst,						// small class icon 
        MAKEINTRESOURCE(5),
        IMAGE_ICON, 
        GetSystemMetrics(SM_CXSMICON), 
        GetSystemMetrics(SM_CYSMICON), 
        LR_DEFAULTCOLOR); 
	if (!RegisterClassEx(&wcx)) {
		ShowError ();
	}


	//hedit is Global
	hedit = CreateWindow("MainWClass",        // name of window class 
						 "Sample",
						 WS_VISIBLE | ES_AUTOHSCROLL | ES_RIGHT,
						 300,110,200,100,
						 NULL,
						 NULL, //(HMENU)ID_ED,
						 hinst, 
						 NULL);
	if (hedit == NULL)  {
		ShowError ();
	}
	else {
		ShowWindow (hedit, SW_SHOW);
		UpdateWindow(hedit);
	}
	*/


	
    hedit = CreateDialog(hinst,
						 MAKEINTRESOURCE(IDD_DEBUG_DIALOG),
						 NULL,
						 (DLGPROC) GoToProc); 
	if (hedit == NULL)  {
		ShowError ();
		return 0;
	}
	else {
		ShowWindow (hedit, SW_SHOW);
	}

	
	while (1) {
		Sleep (10);
	}

	return 0;
}

// CATIAData.cpp: implementation of the CATIAData class.
//
//////////////////////////////////////////////////////////////////


#include "CatiaIncludes.h"
#include "CatiaData.h"
#include "CATError.h"                  // To manage errors in UpdateObject()
#include "CATSystemError.h"
#include "CATErrorMacros.h"            // CATTry, CATCatch, etc.
#include "CATIAModel.h"
#include <vector.h>

namespace DOME {
namespace CATIAPlugin {


/////////////////////////////////////////////////////
// CATIAData: Constructor/deconstructor
/////////////////////////////////////////////////////
CATIAData::CATIAData()
{
	m_pProperties = NULL;
	m_Parm = NULL;
	m_pUserLibrary = NULL;
}

CATIAData::~CATIAData()
{
	if (m_Name != NULL) delete m_Name;
	disconnect();
}


/////////////////////////////////////////////////////
// CATIAData: connect to a specific part parameter
/////////////////////////////////////////////////////
void CATIAData::connect(CATIAPart *spPart)
{
	if (!m_pUserLibrary)
	{
		if (spPart != NULL)
		{

			CATTry 
			{	
				HRESULT hr;
				CATIAParameters *oParameters;	
				hr = spPart->get_Parameters(oParameters);
				if(FAILED(hr) || oParameters == NULL) {
					CATIA_ERROR("CATIAData::connect: Can't get parameter colletion of part.");
					return;
				}

				CATIAParameter * oParameter;
				CATUnicodeString oParaName = m_Name; 
				CATBSTR ooParaName;          
				oParaName.ConvertToBSTR(&ooParaName);
				CATVariant va;
				va.vt = VT_BSTR;
				va.bstrVal = ooParaName;
				hr = oParameters->Item(va, oParameter);
				if(FAILED(hr) || oParameters == NULL) {
					CATIA_ERROR("CATIAData::connect: Can't find the parameter " << m_Name);
					return;
				}

				m_Parm = oParameter;

			}
			// This block treats every other exception
			CATCatch(CATError,error)    {
				cerr << " Error: " << error->GetMessageText() <<endl; 
				Flush(error);
			}
			CATEndTry;

		}
		else
			CATIA_ERROR("CATIAData::connect: CATIAPart pointer is null. Unable to connect.");
	}
}

/*void CATIAData::connect(CATIPrtPart_var spPart)
{
	if (!m_pUserLibrary)
	{
		if (spPart != NULL)
		{
			bool gotReal = false;

			CATTry 
			{	
				CATIParmPublisher_var parmPublisher = spPart;
				CATListValCATISpecObject_var list;
				parmPublisher -> GetAllChildren (CATICkeParm::ClassName(),list);

				CATISpecObject_var specObj;
				for (int k = 1; k <= list.Size(); k++)
				{
					specObj = list[k];
					m_Parm = specObj;

					if (m_Parm == NULL_var) {
						CATIA_ERROR ("CATIAData::connect()"  << endl
									 << "--- Part is NULL. Unable to connect. ---"
									 << endl);
					}
					else
					{
						//**CATIA_DEBUG ("All parameters in the file:" << endl);
						//**CATIA_DEBUG (m_Parm->Name()  << endl);	
					if (m_Parm->Name() == CATUnicodeString (m_Name))
					{
						CATIA_DEBUG (m_Parm->Name() << " = " << m_Parm->Show() << endl);
						gotReal = true;
						break;
					}
					}
				}
			}
			// This block treats every other exception
			CATCatch(CATError,error)    {
				cerr << " Error: " << error->GetMessageText() <<endl; 
				Flush(error);
			}
			CATEndTry;

			if(!gotReal) {
				CATIA_ERROR("CATIAData::connect: Can't find the parameter " << m_Name);
			}
		}
		else
			CATIA_ERROR("CATIAData::connect: CATIPrtPart_var pointer is null. Unable to connect.");
	}
}
*/
/////////////////////////////////////////////////////
// CATIAData: disconnect from a part parameter
/////////////////////////////////////////////////////
void CATIAData::disconnect()
{
	m_Parm = NULL;
	m_pProperties = NULL;
}



/////////////////////////////////////////////////////
// CATIAReal: constructor
/////////////////////////////////////////////////////
CATIAReal::CATIAReal(char *name, UserLibraryManager *pUserLibrary)
{
	if (name != NULL) {
		m_Name = new char[strlen(name)+1];
		strcpy (m_Name, name);
	}
	m_Parm = NULL;
	m_pUserLibrary = pUserLibrary;
}


/////////////////////////////////////////////////////
// CATIAReal: get parameter value
/////////////////////////////////////////////////////
double CATIAReal::getValue()
{
	if (m_pUserLibrary)
	{
		void* value = m_pUserLibrary->getValue (this);
		if (value != NULL)
		{
			m_dValue = *((double*)value);
		}
	}
	else
	if (m_Parm == NULL_var)
	{
		CATIA_ERROR ("CATIAReal::getValue: Part has not been loaded" << endl);
	}
	else 
	{
		//CATIA_DEBUG ("in CATIAReal::getValue");
		CATBSTR oValue;
		m_Parm->ValueAsString(oValue);
		CATUnicodeString ooValue;
		ooValue.BuildFromBSTR(oValue);
		::SysFreeString(oValue);
		ooValue.ConvertToNum(&m_dValue,"%le");
	}
	
	return m_dValue;
}


/////////////////////////////////////////////////////
// CATIAReal: set parameter value
/////////////////////////////////////////////////////
void CATIAReal::setValue(double value)
{
	m_dValue = value;

	if (m_pUserLibrary)
	{
		CATVariant va;
		va.vt = VT_R8;
		va.dblVal = value;

		m_pUserLibrary->setValue (this, va);
	}
	else
	if (m_Parm == NULL_var)
	{
		CATIA_ERROR ("CATIAReal::setValue: Part has not been loaded" << endl);
	}
	else 
	{
		//CATIA_DEBUG ("'" << m_Name << "' old value is " << m_Parm->Show() << endl);
		
		CATTry
		{
			CATUnicodeString iiValue;
			CATBSTR iValue;
			iiValue.BuildFromNum(m_dValue, "%g");
			iiValue.ConvertToBSTR(&iValue);
			m_Parm->ValuateFromString(iValue); //m_Parm->Valuate(m_dValue);
		}
		CATCatchOthers
			CATIA_ERROR ("Valuation error" << endl);
		CATEndTry

		//CATIA_DEBUG ("'" << m_Name << "' new value is " << m_Parm->Show() << endl);
	}
}


/////////////////////////////////////////////////////
// CATIAInteger: constructor
/////////////////////////////////////////////////////
CATIAInteger::CATIAInteger(char *name, UserLibraryManager *pUserLibrary)
{
	if (name != NULL) {
		m_Name = new char[strlen(name)+1];
		strcpy (m_Name, name);
	}
	m_Parm = NULL;
	m_pUserLibrary = pUserLibrary;
}


/////////////////////////////////////////////////////
// CATIAInteger: get parameter value
/////////////////////////////////////////////////////
int CATIAInteger::getValue()
{
	if (m_pUserLibrary)
	{
		void* value = m_pUserLibrary->getValue (this);
		if (value != NULL)
		{
			m_iValue = *((int*)value);
		}
	}
	else
	if (m_Parm == NULL_var)
	{
		CATIA_ERROR ("CATIAInteger::getValue: Part has not been loaded" << endl);
	}
	else 
	{
		//CATIA_DEBUG ("in CATIAInteger::getValue");
		CATBSTR oValue;
		m_Parm->ValueAsString(oValue);
		CATUnicodeString ooValue;
		ooValue.BuildFromBSTR(oValue);
		::SysFreeString(oValue);
		ooValue.ConvertToNum(&m_iValue,"%d");
	}
	
	return m_iValue;
}


/////////////////////////////////////////////////////
// CATIAInteger: set parameter value
/////////////////////////////////////////////////////
void CATIAInteger::setValue(int value)
{
	m_iValue = value;

	if (m_pUserLibrary)
	{
		CATVariant va;
		va.vt = VT_I2;
		va.iVal = value;

		m_pUserLibrary->setValue (this, va);
	}
	else
	if (m_Parm == NULL_var)
	{
		CATIA_ERROR ("CATIAInteger::setValue: Part has not been loaded" << endl);
	}
	else 
	{
		//CATIA_DEBUG ("'" << m_Name << "' old value is " << m_Parm->Show() << endl);
		
		CATTry
		{
			CATUnicodeString iiValue;
			CATBSTR iValue;
			iiValue.BuildFromNum(m_iValue, "%d");
			iiValue.ConvertToBSTR(&iValue);
			m_Parm->ValuateFromString(iValue); //m_Parm->Valuate(m_dValue);
		}
		CATCatchOthers
			CATIA_DEBUG ("Valuation error" << endl);
		CATEndTry

		//CATIA_DEBUG ("'" << m_Name << "' new value is " << m_Parm->Show() << endl);
	}
}


/////////////////////////////////////////////////////
// CATIAString: constructor
/////////////////////////////////////////////////////
CATIAString::CATIAString(char *name, UserLibraryManager *pUserLibrary)
 : m_szValue (NULL)
{
	if (name != NULL) {
		m_Name = new char[strlen(name)+1];
		strcpy (m_Name, name);
	}
	m_Parm = NULL;
	m_pUserLibrary = pUserLibrary;
}

CATIAString::~CATIAString ()
{
	delete[] m_szValue;
}



/////////////////////////////////////////////////////
// CATIAString: get parameter value
/////////////////////////////////////////////////////
const char* CATIAString::getValue()
{
	if (m_pUserLibrary)
	{
		void* value = m_pUserLibrary->getValue (this);
		if (value != NULL)
		{
			delete m_szValue;
			m_szValue = new char[strlen((char*)value)+1];
			strcpy (m_szValue, (char*)value);
		}
	}
	else
	if (m_Parm == NULL_var)
	{
		CATIA_ERROR ("CATIAString::getValue: Part has not been loaded" << endl);
	}
	else 
	{
		//CATIA_DEBUG ("in CATIAString::getValue");

		// get the new string from CATIA
		//CATUnicodeString value = m_Parm->Value()->AsString();
		CATBSTR oValue;
		m_Parm->ValueAsString(oValue);
		CATUnicodeString value;
		value.BuildFromBSTR(oValue);
		::SysFreeString(oValue);

		// make a local copy of the string
		if (value.ConvertToChar() != NULL)
		{
			delete m_szValue;
			m_szValue = new char[strlen(value.ConvertToChar())+1];
			strcpy (m_szValue, value.ConvertToChar());
		}
	}

	return m_szValue;
}


/////////////////////////////////////////////////////
// CATIAString: set parameter value
/////////////////////////////////////////////////////
void CATIAString::setValue(char* value)
{
	if (value != NULL) {
		delete m_szValue;
		m_szValue = new char[strlen(value)+1];
		strcpy (m_szValue, value);
	}

	if (m_pUserLibrary)
	{
		CATUnicodeString ssValue = value;
		CATBSTR sValue;
		ssValue.ConvertToBSTR(&sValue);
		CATVariant va;
		va.vt = VT_BSTR;
		va.bstrVal = sValue;

		m_pUserLibrary->setValue (this, va);
	}
	else
	{
		if (m_Parm == NULL_var)
		{
			CATIA_ERROR ("CATIAString::setValue: Part has not been loaded" << endl);
		}
		else 
		{
			//CATIA_DEBUG ("'" << m_Name << "' old value is " << m_Parm->Show() << endl);

			CATTry
		{
			CATUnicodeString iiValue = value;
			CATBSTR iValue;
			iiValue.ConvertToBSTR(&iValue);
			m_Parm->ValuateFromString(iValue); //m_Parm->Valuate(m_dValue);
		}
			CATCatchOthers
				CATIA_DEBUG ("Valuation error" << endl);
			CATEndTry
			
			//CATIA_DEBUG ("'" << m_Name << "' new value is " << m_Parm->Show() << endl);
		}
	}
}



/////////////////////////////////////////////////////
// CATIABoolean: constructor
/////////////////////////////////////////////////////
CATIABoolean::CATIABoolean(char *name, UserLibraryManager *pUserLibrary)
{
	if (name != NULL) {
		m_Name = new char[strlen(name)+1];
		strcpy (m_Name, name);
	}
	m_Parm = NULL;
	m_pUserLibrary = pUserLibrary;
}


/////////////////////////////////////////////////////
// CATIABoolean: get parameter value
/////////////////////////////////////////////////////
bool CATIABoolean::getValue()
{
	if (m_pUserLibrary)
	{
		void* value = m_pUserLibrary->getValue (this);
		if (value != NULL)
		{
			m_bValue = *((bool*)value);
		}
	}
	else
	if (m_Parm == NULL_var)
	{
		CATIA_ERROR ("CATIABoolean::getValue: Part has not been loaded" << endl);
	}
	else 
	{
		//CATIA_DEBUG ("in CATIABoolean::getValue");
		CATBSTR oValue;
		m_Parm->ValueAsString(oValue);
		CATUnicodeString ooValue;
		ooValue.BuildFromBSTR(oValue);
		::SysFreeString(oValue);
		if(ooValue == "true") m_bValue = true;
		else m_bValue = false;
		
		//CATBoolean value = m_Parm->Value()->AsBoolean();
		//m_bValue = value;
	}
	
	return m_bValue;
}


/////////////////////////////////////////////////////
// CATIABoolean: set parameter value
/////////////////////////////////////////////////////
void CATIABoolean::setValue(bool value)
{
	m_bValue = value;

	if (m_pUserLibrary)
	{
		CATVariant va;
		va.vt = VT_BOOL;
		va.boolVal = value;

		m_pUserLibrary->setValue (this, va);
	}
	else
	if (m_Parm == NULL_var)
	{
		CATIA_ERROR ("CATIABoolean::setValue: Part has not been loaded" << endl);
	}
	else 
	{
		//CATIA_DEBUG ("'" << m_Name << "' old value is " << m_Parm->Show() << endl);

		CATTry
		{
			CATUnicodeString iiValue;
			CATBSTR iValue;
			if(m_bValue == true) iiValue = "true";
			else iiValue = "false";
			iiValue.ConvertToBSTR(&iValue);
			m_Parm->ValuateFromString(iValue); //m_Parm->Valuate(CATBoolean(m_bValue));
		}
			
		CATCatchOthers
			CATIA_DEBUG ("Valuation error" << endl);
		CATEndTry
		
		//CATIA_DEBUG ("'" << m_Name << "' new value is " << m_Parm->Show() << endl);
	}
}


/////////////////////////////////////////////////////
// CATIAVector: constructor and destructor
/////////////////////////////////////////////////////
CATIAVector::CATIAVector(char *name, 
						 UserLibraryManager *pUserLibrary)
{
	if (name != NULL) {
		m_Name = new char[strlen(name)+1];
		strcpy (m_Name, name);
	}
	m_Parm = NULL;
	m_pUserLibrary = pUserLibrary;
}


CATIAVector::~CATIAVector ()
{
	m_vector.clear ();
}


/////////////////////////////////////////////////////
// CATIAVector: get parameter value
/////////////////////////////////////////////////////
vector<double> CATIAVector::getValue ()
{
	m_vector.clear ();

	if (m_pUserLibrary)
	{
		m_vector = m_pUserLibrary->getValueDoubleArray (this);
	}
	else
	{
		// CATIA does not support generic vectors
		CATIA_ERROR ("CATIA does not support vectors" << endl);
	}

	return m_vector;
}


/////////////////////////////////////////////////////
// CATIAVector: set parameter value
/////////////////////////////////////////////////////
void CATIAVector::setValue (unsigned int iNumElements, double *values)
{
	// set new values locally
	m_vector.clear();
	for (unsigned int i = 0; i < iNumElements; i++) {
		m_vector.push_back (values[i]);
	}

	// set new values in user library
	if (m_pUserLibrary)
	{
		m_pUserLibrary->setValueDoubleArray (this, m_vector);
	}
	else
	{
		// CATIA does not support generic vectors
		CATIA_ERROR ("CATIA does not support vectors" << endl);
	}
}



CATIAFile::CATIAFile(char* filePath, char *fileType,
					 UserLibraryManager *pUserLibrary)
: m_FileType (fileType), m_pDoc(NULL)
{
	if (filePath != NULL) {
		m_Name = new char[strlen(filePath)+1];
		strcpy (m_Name, filePath);
	}
	m_pUserLibrary = pUserLibrary;
}

void CATIAFile::connect(CATIADocument *pDoc)   //connect(CATDocument *pDoc)
{
	if (pDoc == NULL) {
		CATIA_ERROR("Unable to connect, pDoc is null.");
	}

	m_pDoc = pDoc;
}

bool CATIAFile::save()
{
	if (m_pDoc == NULL) {
		CATIA_ERROR("CATIAFile::save: pDoc pointer is null. Unable to save file.");
		return false;
	}

	CATTry 
	{	
	    CATUnicodeString outFileName = m_Name; 
	    CATBSTR ooutFileName;         
	    outFileName.ConvertToBSTR(&ooutFileName);
	    CATUnicodeString outTypeName = m_FileType; 
	    CATBSTR ooutTypeName;         
	    outTypeName.ConvertToBSTR(&ooutTypeName);

		m_pDoc->ExportData(ooutFileName, ooutTypeName);
		//	CATDocumentServices::SaveAs(*m_pDoc, m_Name);
		//CATDocumentServices::SaveAs(*m_pDoc, m_Name, m_FileType);
	}
	// This block treats every other exception
	CATCatch(CATError,error)    {
		cerr << " Error: " << error->GetMessageText() <<endl; 
		Flush(error);
	}
	CATEndTry;

	return true;
}


} // namespace CATIAPlugin
} // DOME

// CATIAData.h: interface for the CATIAData class.
//
//////////////////////////////////////////////////////////////////////

#ifndef CATIA_DATA_H
#define CATIA_DATA_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include <vector.h>


namespace DOME {
namespace CATIAPlugin {


class UserLibraryManager;


class CATIAData
{
public:
	CATIAData();
	virtual ~CATIAData();

public:
	virtual void connect(CATIAPart *spPart); //connect(CATIPrtPart_var spPart);
	virtual void disconnect();

protected:
	char* m_Name;
	//CATICkeParm_var m_Parm;
	CATIAParameter *m_Parm;
	CATIMechanicalPartProperties* m_pProperties;
	bool m_bIsUserLibParameter;
	UserLibraryManager *m_pUserLibrary;
};


class CATIAReal : public CATIAData
{

public:
	CATIAReal(char *name, UserLibraryManager *pUserLibrary = NULL);

	double getValue();
	void setValue(double value);

private:
	double m_dValue;
};


class CATIAInteger : public CATIAData
{

public:
	CATIAInteger(char *name, UserLibraryManager *pUserLibrary = NULL);

	int getValue();
	void setValue(int value);

private:
	int m_iValue;
};


class CATIAString : public CATIAData
{

public:
	CATIAString(char *name, UserLibraryManager *pUserLibrary = NULL);
	~CATIAString();

	const char* getValue();
	void setValue(char* value);

private:
	char *m_szValue;
};


class CATIABoolean : public CATIAData
{

public:
	CATIABoolean(char *name, UserLibraryManager *pUserLibrary = NULL);

	bool getValue();
	void setValue(bool value);

private:
	bool m_bValue;
};


class CATIAVector : public CATIAData
{

public:
	CATIAVector(char *name,
				UserLibraryManager *pUserLibrary = NULL);
	~CATIAVector ();

	void			setValue(unsigned int iNumElements, double *values);
	vector<double>	getValue ();

private:
	vector<double> m_vector;
};


class CATIAFile : public CATIAData
{
public:
	CATIAFile(char* filePath, char *fileType,
			  UserLibraryManager *pUserLibrary = NULL);

    bool save();
	void connect(CATIADocument *pDoc = NULL); 

protected:
	CATIADocument *m_pDoc;  //CATDocument *m_pDoc;
	CATUnicodeString m_FileType;
};



} // namespace CATIAPlugin
} // DOME

#endif // CATIA_DATA_H
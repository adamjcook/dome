// DomePlugin.h: interface for the DomePlugin class.
//
//////////////////////////////////////////////////////////////////////

#ifndef DOME_DOMEPLUGIN_H
#define DOME_DOMEPLUGIN_H

namespace DOME {
//namespace DomePlugin {

class DomeData {
  // this is the base class for all data types
};

class DomeReal : public DomeData {
  public:
    // constructor method should take in any necessary parameters
    // destructor should clean up resources

    virtual double getValue() = 0  ;
    virtual void setValue(double val) = 0;
};

class DomeInteger : public DomeData {
  public:
    // constructor method should take in any necessary parameters
    // destructor should clean up resources

    virtual int getValue() = 0  ;
    virtual void setValue(int val) = 0;
};

class DomeString : public DomeData 
{
  public:
    // constructor method should take in any necessary parameters
    // destructor should clean up resources

    virtual string getValue() = 0;
    virtual void setValue(string val) = 0;
};

class DomeBoolean : public DomeData
{
  public:
    // constructor method should take in any necessary parameters
    // destructor should clean up resources

    virtual bool getValue() = 0;
    virtual void setValue(bool val) = 0; 
};

class DomeMatrix : public DomeData 
{
  public:
    // constructor method should take in any necessary parameters
    // destructor should clean up resources

    virtual vector<int> getDimension() = 0;
    virtual void setDimension(int rows, int columns) = 0;
    
    virtual int getRows() = 0;
    virtual void setRows(int rows) = 0;

    virtual int getColumns() = 0;
    virtual void setColumns(int columns) = 0;

    virtual vector < vector<double> > getValues() = 0;
    virtual void setValues(vector < vector<double> > values) = 0;
    virtual double getElement(int row, int column) = 0;
    virtual void setElement(int row, int column, double value) = 0;
};

class DomeVector : public DomeData 
{
  public:
    // constructor method should take in any necessary parameters
    // destructor should clean up resources

    virtual int getDimension() = 0;
    virtual void setDimension(int columns) = 0;

    virtual int getLength() = 0;
    virtual void setLength(int columns) = 0;

    virtual vector<double> getValues() = 0;
    virtual void setValues( vector<double> values) = 0;
    virtual double getElement(int column) = 0;
    virtual void setElement( int column, double value) = 0;
};

// DomeModel is the base class for third-party plugins
class DomeModel {
  public:

    // constructor method should take any parameters
    // necessary to specify model parameters
    DomeModel(){};

    // destructor method should clean up all resources
    // used by the model
    virtual ~DomeModel(){};

    // plugins support various DOME datatypes
    // "factory" methods should be created for each type supported
    // method should create the derived datatype, store a reference to it,
    // and return a pointer to it
    // virtual DomeData* createData() = 0;

    // returns if the model is loaded or not
    // a model is loaded if the third party application is
    // running and all the parameters in the model are
    // "connected" to the application
    virtual bool isModelLoaded() = 0;

    // starts the application, loads the necessary files,
    // connects all the data to the underlying source
    virtual void loadModel() = 0;

    // disconnects data from underlying source,
    // quits the files, closes application as necessary
    virtual void unloadModel() = 0;

    // these are the hooks into the execution process
    // a typical model execution will follow the following sequence:
    // 1. executeBeforeInput
    // 2. set values of inputs
    // 3. execute
    // 4. get values of outputs
    // 5. executeAfterOutput

    virtual void executeBeforeInput() = 0;
    virtual void execute() = 0;
    virtual void executeAfterOutput() = 0;

  protected:
    bool _isModelLoaded;

};

//} // namespace DomePlugin
} // namespace DOME

#endif // DOME_DOMEPLUGIN_H
// ExcelData.cpp: implementation of the ExcelData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ExcelData.h"


COleVariant vOpt(DISP_E_PARAMNOTFOUND, VT_ERROR);


namespace DOME {
	namespace ExcelPlugin {

	////////////////////////////////////////////////////////////////////
	//
	// ExcelData
	//
	////////////////////////////////////////////////////////////////////

	ExcelData::ExcelData(string sheetName, string rangeName) :
		m_sheetName(sheetName), m_rangeName(rangeName) 
	{
	}

	ExcelData::~ExcelData()
	{
		disconnect();
		//cout << "destroy" << endl;
	}

	// connect to the worksheet and get the range of cells
#ifndef _EXCEL_2010
	void ExcelData::connect(Worksheets &xlSheets) throw(DomeException)
	{
		// get the worksheet of interest
		_Worksheet xlSheet = xlSheets.GetItem(COleVariant(m_sheetName.c_str()));
		if (xlSheet == NULL) {
			EXCEL_ERROR("ExcelData::connect: Sheet '"+m_sheetName+"' not found.");
		}

		// get the cells of interest
		try {			
			m_range = xlSheet.GetRange(COleVariant(m_rangeName.c_str()), vOpt);
		} catch (...) {
			disconnect();
			EXCEL_ERROR("ExcelData::connect: Range '"+m_rangeName+"' not found.");
		}

		if (m_range == NULL) {
			EXCEL_ERROR("ExcelData::connect: Range '"+m_rangeName+"' not found.");
		}
	}
#endif

#ifdef _EXCEL_2010
	void ExcelData::connect(CWorksheets &xlSheets) throw(DomeException)
	{
		// get the worksheet of interest
		//CString xlSheet = xlSheets.get_Name(CString(m_sheetName.c_str()));
		CWorksheet xlSheet = xlSheets.get_Item(COleVariant(m_sheetName.c_str()));

		if (xlSheet == NULL) {
			EXCEL_ERROR("ExcelData::connect: Sheet '"+m_sheetName+"' not found.");
		}

		// get the cells of interest
		try {			
			m_range = xlSheet.get_Range(COleVariant(m_rangeName.c_str()), vOpt);
		} catch (...) {
			disconnect();
			EXCEL_ERROR("ExcelData::connect: Range '"+m_rangeName+"' not found.");
		}

		if (m_range == NULL) {
			EXCEL_ERROR("ExcelData::connect: Range '"+m_rangeName+"' not found.");
		}
	}
#endif




	// disconnect from the range of cells
	void ExcelData::disconnect()
	{		
		m_range.ReleaseDispatch();
	}



	////////////////////////////////////////////////////////////////////
	//
	// ExcelReal
	//
	////////////////////////////////////////////////////////////////////

	ExcelReal::ExcelReal(string sheetName, string rangeName) :
		ExcelData(sheetName, rangeName) 
	{
	}

	// get real value
	double ExcelReal::getValue() throw(DomeException)
	{
		COleVariant result;

		// retrieve the value
		try {
#ifndef _EXCEL_2010
			result = m_range.GetValue();
#else
			result = m_range.get_Value2();
#endif 
		} catch (...) {
			EXCEL_ERROR("ExcelReal::getValue: '"+m_sheetName+"!"+m_rangeName+"'.");
		}

		// convert the value
		try {
			result.ChangeType (VT_R8);
			double value = result.dblVal;
			return value;
		} catch (...) {
			EXCEL_ERROR("ExcelReal::getValue: '"+m_sheetName+"!"+m_rangeName+"' not real value.");
		} 
	}


	// set real value
	void ExcelReal::setValue(double value) throw(DomeException)
	{
		try {
			
#ifndef _EXCEL_2010
			m_range.SetValue(COleVariant(value));
#else
			m_range.put_Value2(COleVariant(value));
#endif 


		} catch (...) {
			EXCEL_ERROR("ExcelReal::setValue: to "+str(value)+".");
		}
	}



	/////////////////////////////////////////////////////
	// ExcelString
	////////////////////////////////////////////////////

	ExcelString::ExcelString(string sheetName, string rangeName) : 
		ExcelData(sheetName, rangeName)
	{
	}


	/////////////////////////////////////////////////////
	// ExcelString: get parameter value
	/////////////////////////////////////////////////////

	string ExcelString::getValue()
	{
		COleVariant result;

		// retrieve the value
		try {
			
#ifndef _EXCEL_2010
			result = m_range.GetValue();
#else
			result = m_range.get_Value2();
#endif 
		} catch (...) {
			EXCEL_ERROR("ExcelString::getValue: '"+m_sheetName+"!"+m_rangeName+"'.");
		}

		// convert the value
		try {
			result.ChangeType (VT_BSTR);
						
			CString tmp = result.bstrVal;
			CT2CA pszConvertedAnsiString (tmp);
			string value (pszConvertedAnsiString);

			return value;
		} catch (...) {
			EXCEL_ERROR("ExcelString::getValue: '"+m_sheetName+"!"+m_rangeName+"' not a string.");
		} 
	}


	/////////////////////////////////////////////////////
	// ExcelString: set parameter value
	/////////////////////////////////////////////////////

	void ExcelString::setValue(string value)
	{
		try {
#ifndef _EXCEL_2010
			m_range.SetValue(COleVariant(value.c_str()));
#else
			m_range.put_Value2(COleVariant(value.c_str()));
#endif 

		} catch (...) {
			EXCEL_ERROR("ExcelString::setValue: to "+value+".");
		}
	}


	////////////////////////////////////////////////////////////////////
	//
	// ExcelMatrix
	//
	////////////////////////////////////////////////////////////////////

	ExcelMatrix::ExcelMatrix(string sheetName, string rangeName) : 
		ExcelData(sheetName, rangeName)
	{
	}


	// get matrix dimensions
	vector<int> ExcelMatrix::getDimension()
	{
		try {
			vector<int> dim;
			dim.push_back(getRows());
			dim.push_back(getColumns());
			return dim;
		} catch (...) {
			throw;
		}
	}


	// set matrix dimensions
	void ExcelMatrix::setDimension(int rows, int columns)
	{
		DOME_NOT_SUPPORTED("ExcelMatrix::setDimension");
	}

#ifndef _EXCEL_2010
	// get number of matrix rows
	int ExcelMatrix::getRows() throw(DomeException)
	{
		Range rangeRows;
		try {
			rangeRows = m_range.GetRows();
			long rows = rangeRows.GetCount();
			return rows;
		} catch (...) {
			EXCEL_ERROR("ExcelMatrix::getRows");
		}
	}
#endif
#ifdef _EXCEL_2010
	// get number of matrix rows
	int ExcelMatrix::getRows() throw(DomeException)
	{
		CRange rangeRows;
		try {
			rangeRows = m_range.get_Rows();
			long rows = rangeRows.get_Count();
			return rows;
		} catch (...) {
			EXCEL_ERROR("ExcelMatrix::getRows");
		}
	}
#endif


	// set number of matrix rows
	void ExcelMatrix::setRows(int rows) throw(DomeException)
	{
		DOME_NOT_SUPPORTED("ExcelMatrix::setRows");
	}

#ifndef _EXCEL_2010
	// get number of matrix columns
	int ExcelMatrix::getColumns() throw(DomeException)
	{
		Range rangeColumns;
		try {
			rangeColumns = m_range.GetColumns();
			long columns = rangeColumns.GetCount();
			return columns;
		} catch (...) {
			EXCEL_ERROR("ExcelMatrix::getColumns");
		}
	}
#endif
#ifdef _EXCEL_2010
	// get number of matrix columns
	int ExcelMatrix::getColumns() throw(DomeException)
	{
		CRange rangeColumns;
		try {
			rangeColumns = m_range.get_Columns();
			long columns = rangeColumns.get_Count();
			return columns;
		} catch (...) {
			EXCEL_ERROR("ExcelMatrix::getColumns");
		}
	}
#endif


	// set number of matrix columns
	void ExcelMatrix::setColumns(int columns) throw(DomeException)
	{
		DOME_NOT_SUPPORTED("ExcelMatrix::setColumns");
	}


	// get matrix values
	vector< vector<double> > ExcelMatrix::getValues()
	{
		try {
#ifndef _EXCEL_2010
			COleVariant values = m_range.GetValue();
#else
			COleVariant values = m_range.get_Value2();
#endif
			vector <vector <double> > valuesVector;

			if (::SafeArrayGetDim(values.parray) == 2)	// is Matrix
			{
				long rows, cols;
				::SafeArrayGetUBound(values.parray, 1, &rows);
				::SafeArrayGetUBound(values.parray, 2, &cols);
				for(int i=1; i<=rows; i++) 
				{
					std::vector<double> rowVector;
					for(int j=1; j<=cols; j++)
					{
						COleVariant doubleResult;
						long indices[] = {i,j};
						::SafeArrayGetElement(values.parray, indices, LPVARIANT(doubleResult));
						doubleResult.ChangeType (VT_R8);
						rowVector.push_back(doubleResult.dblVal);
					}
					valuesVector.push_back(rowVector);
				}   
				return valuesVector;
			} else {
				EXCEL_ERROR("ExcelMatrix::getValues: Dimension error "+str((int)::SafeArrayGetDim(values.parray)));
			}
		} catch (...) {
			EXCEL_ERROR("ExcelMatrix::getValues");
		}

		/*
		try {
			VARIANT values = m_range.GetValue();
			vector <vector <double> > valuesVector;

			if (::SafeArrayGetDim(values.parray) == 2) { // is Matrix
				long rows, cols;
				::SafeArrayGetUBound(values.parray,1,&rows);
				::SafeArrayGetUBound(values.parray,2,&cols);
				for(int i=1; i<=rows; i++) {
					std::vector<double> rowVector;
					for(int j=1; j<=cols; j++) {
						VARIANT tmp;
						VariantInit(&tmp);
						long indices[] = {i,j};
						::SafeArrayGetElement(values.parray, indices, &tmp);
						VARIANT doubleResult;
						VariantInit(&doubleResult);
						VariantChangeType(&doubleResult,&tmp,NULL,VT_R8);
						rowVector.push_back(doubleResult.dblVal);
						VariantClear(&tmp);
						VariantClear(&doubleResult);
					}
					valuesVector.push_back(rowVector);
				}   
				return valuesVector;
			} else {
				EXCEL_ERROR("ExcelMatrix::getValues: Dimension error "+str((int)::SafeArrayGetDim(values.parray)));
			}
		} catch (...) {
			EXCEL_ERROR("ExcelMatrix::getValues");
		}
		*/
	}


	// set matrix values
	void ExcelMatrix::setValues(vector< vector<double> > values)
	{
		try {
			int rows = getRows();
			int columns = getColumns();

			if (values.size() < 0 || values.size() > rows)
			{
				EXCEL_ERROR("ExcelMatrix::setValues: invalid row dimension"+str((int)values.size()));
			}

			for (int i=0; i<values.size(); i++)
			{
				if (values[i].size() < 0 || values[i].size() > columns)
				{
					EXCEL_ERROR("ExcelMatrix::setValues: invalid column dimension"+str((int)values[i].size()));
				}
			}

			COleVariant arr;
			arr.ChangeType (VT_ARRAY | VT_R8);
			SAFEARRAYBOUND sab[2];
			sab[0].lLbound = 1; sab[0].cElements = rows;
			sab[1].lLbound = 1; sab[1].cElements = columns;
			arr.parray = ::SafeArrayCreate(VT_R8, 2, sab);

			// Fill safearray with values...
			double tmp;
			for(int i=1; i<=rows; i++) {
				for(int j=1; j<=columns; j++) {
					// Create entry value for (i,j)
					tmp = values[i-1][j-1];
					// Add to safearray...
					long indices[] = {i,j};
					::SafeArrayPutElement(arr.parray, indices, &tmp);
				}
			}
#ifndef _EXCEL_2010
			m_range.SetValue(arr);
#else
			m_range.put_Value2(arr);
#endif
			::SafeArrayDestroy(arr.parray);

			/*
			VARIANT arr;
			arr.vt = VT_ARRAY | VT_R8;
			SAFEARRAYBOUND sab[2];
			sab[0].lLbound = 1; sab[0].cElements = rows;
			sab[1].lLbound = 1; sab[1].cElements = columns;
			arr.parray = ::SafeArrayCreate(VT_R8, 2, sab);

			// Fill safearray with values...
			double tmp;
			for(i=1; i<=rows; i++) {
				for(int j=1; j<=columns; j++) {
					// Create entry value for (i,j)
					tmp = values[i-1][j-1];
					// Add to safearray...
					long indices[] = {i,j};
					::SafeArrayPutElement(arr.parray, indices, &tmp);
				}
			}
			m_range.SetValue(arr);
			::SafeArrayDestroy(arr.parray);
			VariantClear(&arr);
			*/
		} catch (DomeException) {
			throw;
		} catch (...) {
			EXCEL_ERROR("ExcelMatrix::setValues");
		}
	}


	// get matrix element
	double ExcelMatrix::getElement(int row, int column)
	{
		try {
			// get the data and check the dimensions
#ifndef _EXCEL_2010
			COleVariant values = m_range.GetValue ();
#else
			COleVariant values = m_range.get_Value2();
#endif
			if (::SafeArrayGetDim(values.parray) != 2) 
			{
				EXCEL_ERROR("ExcelMatrix::getValues: Dimension error "+str((int)::SafeArrayGetDim(values.parray)));
			}

			// get the desired element from the array
			double value = 0;
			COleVariant doubleResult;
			long indices[] = {row+1, column+1};
			HRESULT hr = ::SafeArrayGetElement (values.parray, indices, LPVARIANT(doubleResult));
			if (!FAILED(hr)) {
				doubleResult.ChangeType (VT_R8);
				value = doubleResult.dblVal;
			}

			return value;
		} catch (...) {
			EXCEL_ERROR("ExcelMatrix::getValue: ["+str(row)+","+str(column)+"].");
		}

		/*
		try {
			VARIANT values = m_range.GetValue();

			if (::SafeArrayGetDim(values.parray) != 2) 
			{
				EXCEL_ERROR("ExcelMatrix::getValues: Dimension error "+str((int)::SafeArrayGetDim(values.parray)));
			}

			long rows, cols;
			::SafeArrayGetUBound(values.parray,1,&rows);
			::SafeArrayGetUBound(values.parray,2,&cols);

			VARIANT tmp, doubleResult;
			VariantInit(&tmp);
			VariantInit(&doubleResult);
			long indices[] = {row+1, column+1};
			::SafeArrayGetElement(values.parray, indices, &tmp);
			VariantChangeType(&doubleResult, &tmp, NULL, VT_R8);

			double value = doubleResult.dblVal;

			VariantClear(&tmp);
			VariantClear(&doubleResult);

			return value;
		} catch (...) {
			EXCEL_ERROR("ExcelMatrix::getValue: ["+str(row)+","+str(column)+"].");
		}
		*/
	}


	// set matrix element
	void ExcelMatrix::setElement(int row, int column, double value)
	{
		if (row < 0 || column < 0 || row >= getRows() || column >= getColumns())
		{
			EXCEL_ERROR("ExcelMatrix::setElement: invalid row or column");
		}

		try {
			// convert from 0-based matrix to 1-based matrix
			COleVariant vRow((long)(row+1));
			COleVariant vCol((long)(column+1));
			COleVariant vValue(value);
#ifndef _EXCEL_2010
			m_range.SetItem (vRow, vCol, vValue);
#else
			m_range.put_Item(vRow, vCol, vValue);
#endif
		} catch (...) {
			EXCEL_ERROR("ExcelMatrix::setElement: ["+str(row)+","+str(column)+"] to "+str(value)+".");
		}

		/*
		if (row < 0 || column < 0 || row >= getRows() || column >= getColumns())
		{
			EXCEL_ERROR("ExcelMatrix::setElement: invalid row or column");
		}

		try {
			// convert from 0-based matrix to 1-based matrix
			VARIANT vRow;
			vRow.vt = VT_I4;
			vRow.lVal = row+1;
			VARIANT vColumn;
			vColumn.vt = VT_I4;
			vColumn.lVal = column+1;
			VARIANT vValue;
			vValue.vt = VT_R8;
			vValue.dblVal = value;
			m_range.SetItem(vRow,vColumn,vValue);
			VariantClear(&vRow);
			VariantClear(&vColumn);
			VariantClear(&vValue);
		} catch (...) {
			EXCEL_ERROR("ExcelMatrix::setElement: ["+str(row)+","+str(column)+"] to "+str(value)+".");
		}
		*/

	}

	} // namespace ExcelPlugin
} // namespace DOME
// ExcelModel.h: interface for the ExcelModel class.
//
//////////////////////////////////////////////////////////////////////
#ifndef DOME_EXCELMODEL_H
#define DOME_EXCELMODEL_H


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ExcelData.h"

namespace DOME {
namespace ExcelPlugin {
class ExcelData;
class ExcelReal;
class ExcelString;
class ExcelMatrix;
//class ExcelVisible;


class ExcelModel : public DomeModel  
{
public:
	ExcelModel(string fileName, bool _isVisible) throw(DomeException);
	virtual ~ExcelModel();

	ExcelReal* createReal(string sheetName, string rangeName) throw(DomeException);
	ExcelString* createString(string sheetName, string rangeName) throw(DomeException);
	ExcelMatrix* createMatrix(string sheetName, string rangeName) throw(DomeException);
//	ExcelVisible* createVisible() throw(DomeException);

	bool isModelLoaded();
	void loadModel() throw(DomeException);
	void unloadModel() throw(DomeException);

	void executeBeforeInput() throw(DomeException) {};
    void execute() throw(DomeException);
    void executeAfterOutput() throw(DomeException) {};

private:
	string m_filename;
	//ExcelVisible* m_isVisible;
	bool m_isVisible;
	vector <ExcelData*> m_data;

	_Application m_xlApp;
	_Workbook m_xlBook;

	bool m_isModelLoaded;
};

} // namespace ExcelPlugin
} // DOME

#endif // DOME_EXCELMODEL_H

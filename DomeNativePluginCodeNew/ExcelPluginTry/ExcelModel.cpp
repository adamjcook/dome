// ExcelModel.cpp: implementation of the ExcelModel class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ExcelModel.h"
#include <objbase.h>


namespace DOME {
namespace ExcelPlugin {


COleVariant vTrue((short)TRUE),							// "true" parameter
			vFalse((short)FALSE),						// "false" parameter
			vOpt((long)DISP_E_PARAMNOTFOUND, VT_ERROR);	// optional parameter value


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ExcelModel::ExcelModel(string filename, bool isVisible) throw(DomeException) :
    m_filename(filename), m_isVisible (isVisible), m_isModelLoaded (false)
{
}

ExcelModel::~ExcelModel()
{
	//cout << "in destructor" << endl;

	unloadModel();

	// disconnect from all the sheets
	for (int i = 0; i < m_data.size(); i++)
	{
		delete m_data[i];
	}
}

bool ExcelModel::isModelLoaded()
{
	return m_isModelLoaded;
}

void ExcelModel::loadModel() throw(DomeException)
{
	try {
		if (isModelLoaded()) return;

		// Initialize COM library
		CoInitializeEx(NULL, COINIT_MULTITHREADED);

		// Get a handle to the Excel application
		COleException ex;
		/* the following could be done to detect different versions at runtime
		bool is97 = false;
		if (m_xlApp.CreateDispatch("Excel.Application.8", &ex)) {
			is97 = true;
		}
		else
		// office2K is "Excel.Application.9"
		// officeXP is "Excel.Application.10"
		*/
		if (!m_xlApp.CreateDispatch("Excel.Application", &ex))
			
		{
			char msg[_MAX_PATH], msg2[_MAX_PATH];
			ex.GetErrorMessage (msg, _MAX_PATH-1);
			sprintf (msg2, "Cannot start Excel.\n%s", msg);
			AfxMessageBox(msg2);
			return;
		}

		// Set the application visibile/not visible
		m_xlApp.SetVisible(m_isVisible);

		// Get the workbooks. 
		Workbooks xlBooks = NULL;
		xlBooks = m_xlApp.GetWorkbooks();
		if (xlBooks == NULL) {
			unloadModel();
			EXCEL_ERROR("ExcelModel::loadModel: Unable to GetWorkbooks."); 
		}

		// Open Workbook
		m_xlBook = xlBooks.Open(m_filename.c_str(), // workbook filename
								vTrue,				// update links
								vTrue,				// open read only
								vOpt, vOpt, vOpt, vOpt, vOpt,
								vOpt, vOpt, vOpt, vOpt, vOpt);
		if (m_xlBook == NULL) {
			unloadModel();
			EXCEL_ERROR("ExcelModel::loadModel: Unable to open workbook.");
		}


		// Hack to close extra book in Excel 2000
	#ifdef XL_2000
		for (int i = xlBooks.GetCount(); i > 0; i--)
		{
			_Workbook pbook = NULL;
			try {
				pbook = xlBooks.GetItem(COleVariant((short)i));
			} catch (...) {
				EXCEL_ERROR("ExcelModel::loadModel: Unable to GetItem(vbook).");
			}

			if (pbook != m_xlBook)
			{
				try {
					pbook.Close(vFalse,	// do not save changes
								vOpt, vOpt);
				} catch (...) {
					EXCEL_ERROR("ExcelModel::loadModel: Unable to Close().");
				}
			}
		}

	#endif


		// get all the worksheets in the workbook
		Worksheets xlSheets;
		try {
			xlSheets = m_xlBook.GetSheets();
		} catch (...) {
			unloadModel();
			EXCEL_ERROR("ExcelModel::loadModel: Unable to GetSheets.");
		}
		if(xlSheets == NULL) { 
			unloadModel();
			EXCEL_ERROR("ExcelModel::loadModel: Unable to GetSheets."); 
		}


		// connect to all the sheets
		for (int j = 0; j < m_data.size(); j++)
		{
			m_data[j]->connect(xlSheets);
		}


	} catch (DomeException e) {
		char msg[_MAX_PATH];
		sprintf (msg, "ExcelModel::loadModel\n\n%s", e.what());
        EXCEL_ERROR(msg);
		throw;
	}
	catch (...) {
		EXCEL_ERROR("ExcelModel::loadModel");
	}


	// indicate that the model has been loaded
	m_isModelLoaded = true;
}


void ExcelModel::unloadModel() throw(DomeException)
{
	if (isModelLoaded())
	{
		// disconnect from all the sheets
		for (int i = 0; i < m_data.size(); i++)
		{
			m_data[i]->disconnect();
		}

		
		// Close the workbook. 
		try {
			m_xlBook.Close(vFalse,	// do not save changes
						   vOpt, vOpt);
		} catch (...) {
			EXCEL_ERROR("ExcelModel::unloadModel: Close() failed.");
		}

		// quit the application
		m_xlApp.Quit();
		m_xlApp.ReleaseDispatch ();

		CoUninitialize ();
	}

	m_isModelLoaded = false;
}

void ExcelModel::execute() throw(DomeException)
{
	/* the following would be used if the calculation mode was set to manual	
		try {
			_xlApp.Calculate();
		} catch (...) {
			throw DomeException(__FILE__, __LINE__, "Unknown exception in ExcelModel::execute()");
		}
	*/
}


ExcelReal* ExcelModel::createReal(string sheetName, string rangeName) throw(DomeException)
{
	ExcelReal* real = new ExcelReal(sheetName, rangeName);
	m_data.push_back(real);
	return real;
}


ExcelString* ExcelModel::createString(string sheetName, string rangeName) throw(DomeException)
{
	ExcelString* string = new ExcelString(sheetName, rangeName);
	m_data.push_back(string);
	return string;
}


ExcelMatrix* ExcelModel::createMatrix(string sheetName, string rangeName) throw(DomeException)
{
	ExcelMatrix* matrix = new ExcelMatrix(sheetName, rangeName);
	m_data.push_back(matrix);
	return matrix;
}


} // namespace ExcelPlugin
} // DOME

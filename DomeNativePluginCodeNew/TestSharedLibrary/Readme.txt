This project is a working demonstrating of the concept of adapting the
DOME plug-in architecture to a Linux environment. It uses JNI to communicate
with a shared library object (the analog to a Windows DLL) written in C.

The example sends two numbers to the plug-in (i.e., the shared library),
which returns the sum of the numbers.

The Java code is in ./src. It consists of the class Main, which drives the
program, and the class NativeCaller that provides the JNI interface. These
files can be built in IDEA.

The C code is in NativeCaller.h and NativeCaller.c. Type 'make' to build the
shared library, which is called libNativeCaller.so. The 'lib' prefix is necessary
for JNI to work properly.

For the example to work, the shell variable LD_LIBRARY_PATH must include the path
where the shared library object resides. For example,

export LD_LIBRARY_PATH=/dome3/development/nativeplugincode/TestSharedLibrary

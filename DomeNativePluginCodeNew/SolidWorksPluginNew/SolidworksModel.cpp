// SolidworksModel.cpp: implementation of the SolidworksModel class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#include "SolidworksModel.h"
#include <istream.h>

namespace DOME {
namespace SolidworksPlugin {

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

// MUST IMPLEMENT DEFAULT CONSTRUCTOR IN THE FUTURE

SolidworksModel::SolidworksModel(string filename, bool isVisible) throw(DomeException)
{
	_filename = filename;
	_isVisible = isVisible;
	_swApp = NULL;
	_modelPtr = NULL;
	_model = NULL;

}

SolidworksModel::~SolidworksModel()
{
	unloadModel();
}

bool SolidworksModel::isModelLoaded() 
{
	if (_swApp != NULL && _modelPtr != NULL && _model != NULL) return true;
	return false;
}

void SolidworksModel::loadModel() throw(DomeException)
{
	if (isModelLoaded()) return;
	
	CoInitializeEx(NULL, 0x0);

    // Get CLSID for our server...
	CLSID	clsid;
	HRESULT hr;

    // get CLSID for SolidWorks
    hr = CLSIDFromProgID(L"SldWorks.Application", &clsid);
	if(FAILED(hr)) { 
		unloadModel();
		SOLIDWORKS_ERROR("SolidworksModel::loadModel: CLSIDFromProgID() failed.");
	}
	
    IUnknown* pUnk = NULL;
    hr = CoCreateInstance(clsid, NULL, CLSCTX_LOCAL_SERVER, IID_IUnknown, (void **)&pUnk);
    if(FAILED(hr)) { 
		unloadModel();
		SOLIDWORKS_ERROR("SolidworksModel::loadModel: SolidWorks not registered properly.");
	}
	
    IDispatch* pDisp;
    hr = pUnk->QueryInterface(IID_IDispatch, (void **)&pDisp);
	
	_swApp = new ISldWorks(pDisp);
	pUnk->Release(); 

	if (_isVisible == true)
		_swApp->SetVisible(VARIANT_TRUE); 
	else
		_swApp->SetVisible(VARIANT_FALSE); 
	
	if (_swApp->LoadAddIn ("sldxgl.dll") < 0) {
		unloadModel();
		SOLIDWORKS_ERROR("SolidworksModel::loadModel: XGL Add-In failed to load.");
	}
	
	swDocumentTypes_e type;
	if (_isFilenamePart(_filename))
	{
		type = swDocPART;
	} 
	else if (_isFilenameAssembly(_filename))
	{
		type = swDocASSEMBLY;
	} 
	else if (_isFilenameDrawing(_filename))
	{
		type = swDocDRAWING;
	}
	else 
	{
		unloadModel();
		SOLIDWORKS_ERROR("SolidworksModel::loadModel: Unknown file type encountered.");
	}

	bool readOnly = false;
	bool viewOnly = false;
	bool RapidDraft = false;
	bool silent = true;
	long error;

	_modelPtr = _swApp->OpenDoc3(_filename.c_str(), type, readOnly, viewOnly, RapidDraft, silent, &error);
    if (_modelPtr == NULL) {
		unloadModel();
		string message = "Error opening file ";
		message +=_filename;
		message +=" error is ";
		message += error;
		message += ".";
		SOLIDWORKS_ERROR("SolidworksModel::loadModel: "+message);
    }
	
	_model = new IModelDoc(_modelPtr);
	if (_model == NULL){
		unloadModel();
		SOLIDWORKS_ERROR("SolidworksModel::loadModel: Model is null in readPublisherFile().");
	}

	_createConnections();
	SOLIDWORKS_DEBUG("Solidworks model "<<_filename.c_str()<<" successfully loaded.");
}

void SolidworksModel::unloadModel() throw(DomeException) //this is called when Solidworks can't be started up or the file can't be opened.
{
	_destroyConnections();

	//**delete _model;
	//**delete _swApp;

	_model = NULL;
	_modelPtr = NULL;
	//**_swApp = NULL;
	//**CoUninitialize();

//	while (!_data.empty())
//		_data.pop_back();

	//SOLIDWORKS_DEBUG("Solidworks model "<<_filename.c_str()<<" successfully unloaded.");
	
}

void SolidworksModel::unloadModel(int numModels) throw(DomeException)
{
	cout << "Model number: " << numModels<< endl;

	if (isModelLoaded())
	{

	_destroyConnections();
	if (numModels == 1 && _swApp != NULL)
	{
		_swApp->CloseDoc(_filename.c_str());
		_swApp->ExitApp();
	}
	delete _model;
	delete _swApp;

	_model = NULL;
	_modelPtr = NULL;
	_swApp = NULL;
	CoUninitialize();

//	while (!_data.empty())
//		_data.pop_back();

	SOLIDWORKS_DEBUG("Solidworks model "<<_filename.c_str()<<" successfully unloaded.");
	}
}

void SolidworksModel::execute() throw(DomeException)
{
	SOLIDWORKS_DEBUG("Entering SolidworksModule::execute");
	if(_model->GetType() == swDocPART)
	{
		IPartDoc part(_modelPtr);
		_modelPtr->AddRef();
		part.EditRebuild();
	}
	else if(_model->GetType() == swDocASSEMBLY)
	{
		IAssemblyDoc assembly(_modelPtr);
		_modelPtr->AddRef();
		assembly.EditRebuild();
	}
	else if(_model->GetType() == swDocDRAWING)
	{
		IDrawingDoc drawing(_modelPtr);
		_modelPtr->AddRef();
		drawing.EditRebuild();
	}
     
    for (int i=0; i<_files.size();i++) 
	{
	    SolidworksFile* fil = _files.at(i);
		//fil->connect(_model);
		fil->save();
		//fil->disconnect();
	} 


	SOLIDWORKS_DEBUG("Leaving SolidworksModule::execute");
}

SolidworksDimension* SolidworksModel::createDimension(string dimensionName) throw(DomeException)
{
	SolidworksDimension* dim = new SolidworksDimension(dimensionName);
	_data.push_back(dim);
	return dim;
}

SolidworksFile* SolidworksModel::createFile(string fileName) throw(DomeException)
{
	SolidworksFile* fil = new SolidworksFile(fileName);
	_files.push_back(fil);
	return fil;
}

SolidworksLengthUnit* SolidworksModel::createLengthUnit() throw(DomeException)
{
	SolidworksLengthUnit* unit = new SolidworksLengthUnit();
	_data.push_back(unit);
	return unit;
}

SolidworksAngleUnit* SolidworksModel::createAngleUnit() throw(DomeException)
{
	SolidworksAngleUnit* unit = new SolidworksAngleUnit();
	_data.push_back(unit);
	return unit;
}

SolidworksMass* SolidworksModel::createMass() throw(DomeException)
{
	SolidworksMass* mass = new SolidworksMass();
	_data.push_back(mass);
	return mass;
}

SolidworksVolume* SolidworksModel::createVolume() throw(DomeException)
{
	SolidworksVolume* volume = new SolidworksVolume();
	_data.push_back(volume);
	return volume;
}

SolidworksSurfaceArea* SolidworksModel::createSurfaceArea() throw(DomeException)
{
	SolidworksSurfaceArea* area = new SolidworksSurfaceArea();
	_data.push_back(area);
	return area;
}

SolidworksColors* SolidworksModel::createColors() throw(DomeException)
{
	SolidworksColors* colors = new SolidworksColors();
	_data.push_back(colors);
	return colors;
}


void SolidworksModel::_createConnections() throw(DomeException)
{
	try
	{
		for (int i=0; i<_data.size(); i++)
		{
			_data[i]->connect(_model);
		}
		for (int j=0; j<_files.size(); j++)
		{
			_files[j]->connect(_model);
		}
	} catch(...) {
		throw;
	}
}

void SolidworksModel::_destroyConnections() throw(DomeException)
{
	try
	{
		for (int i=0; i<_data.size(); i++)
		{
			_data[i]->disconnect();
		}
		for (int j=0; j<_files.size(); j++)
		{
			_files[j]->disconnect();
		}
	} catch(...) {
		throw;
	}
}

bool SolidworksModel::_isFilenamePart(string filename) 
{
	if (filename.find(".SLDPRT") != string::npos || 
		filename.find(".sldprt") != string::npos || 
		filename.find(".prt")	 != string::npos || 
		filename.find(".PRT")	 != string::npos )
	{
		return true;
	}
	return false;
}

bool SolidworksModel::_isFilenameAssembly(string filename) 
{
	if (filename.find(".SLDASM")    != string::npos || 
		filename.find(".sldasm")	!= string::npos || 
		filename.find(".asm")		!= string::npos || 
		filename.find(".ASM")		!= string::npos )
	{
		return true;
	}
	return false;
}


bool SolidworksModel::_isFilenameDrawing(string filename) 
{
	if (filename.find(".SLDDRW")    != string::npos || 
		filename.find(".slddrw")	!= string::npos || 
		filename.find(".drw")		!= string::npos || 
		filename.find(".DRW")		!= string::npos )
	{
		return true;
	}
	return false;
}

} //namespace SolidworksPlusin
} //DOME
// SolidworksModel.h: interface for the SolidworksModel class.
//
//////////////////////////////////////////////////////////////////////

#ifndef SOLIDWORKS_MODEL_H
#define SOLIDWORKS_MODEL_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SolidworksData.h"

namespace DOME {
namespace SolidworksPlugin {
	
class SolidworksData;
class SolidworksDimension;
class SolidworksFile;
class SolidworksUnit;
class SolidworksLengthUnit;
class SolidworksAngleUnit;
class SolidworksMass;
class SolidworksVolume;
class SolidworksSurfaceArea;
class SolidworksColors;

class SolidworksModel : public DomeModel
{
public:
	SolidworksModel() throw(DomeException);
	SolidworksModel(string fileName, bool isVisible=false) throw(DomeException);
	virtual ~SolidworksModel();

	SolidworksDimension* createDimension(string dimensionName) throw(DomeException);
	SolidworksFile* createFile(string fileName) throw(DomeException);
	SolidworksLengthUnit* createLengthUnit() throw(DomeException);
	SolidworksAngleUnit* createAngleUnit() throw(DomeException);
	SolidworksMass* createMass() throw(DomeException);
	SolidworksVolume* createVolume() throw(DomeException);
	SolidworksSurfaceArea* createSurfaceArea() throw(DomeException);
	SolidworksColors* createColors() throw(DomeException);

	bool isModelLoaded();
	void loadModel() throw(DomeException);
	void unloadModel() throw(DomeException);
	void unloadModel(int numModels) throw(DomeException);

	void executeBeforeInput() throw(DomeException) {};
    void execute() throw(DomeException);
    void executeAfterOutput() throw(DomeException) {};

private:
	string _filename;
	bool _isVisible;
	vector <SolidworksData*> _data;
	vector <SolidworksFile*> _files;

	ISldWorks* _swApp;
	LPDISPATCH _modelPtr;
	IModelDoc* _model;
	
	void _createConnections() throw(DomeException);
	void _destroyConnections() throw(DomeException);

	bool _isFilenamePart(string filename);
	bool _isFilenameAssembly(string filename);
	bool _isFilenameDrawing(string filename);

};

} // namespace SolidworksPlugin
} // DOME

#endif // SOLIDWORKS_MODEL_H

// SolidworksData.h: interface for the SolidworksData class.
//
//////////////////////////////////////////////////////////////////////

#ifndef SOLIDWORKS_DATA_H
#define SOLIDWORKS_DATA_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SolidworksIncludes.h"
#include "SolidworksModel.h"

namespace DOME {
namespace SolidworksPlugin {
//class SolidworksModel;

class SolidworksData
{
public:
	SolidworksData();
	virtual ~SolidworksData();

protected:
	friend class SolidworksModel;
	virtual void connect(IModelDoc* model) throw(DomeException); 
	virtual void disconnect();
	
	IModelDoc* _swModel;
};

class SolidworksDimension : public SolidworksData, public DomeReal
{
//friend class SolidworksModel;

public:
	SolidworksDimension(string dimensionName);
	virtual ~SolidworksDimension();

	double getValue() throw(DomeException);
	void setValue(double value) throw(DomeException);

private:
	void connect(IModelDoc* model) throw(DomeException); 
	void disconnect();

	string _dimensionName;
	IDimension* _dimension;
};

//***********************SolidworksFile********************
class SolidworksFile : public SolidworksData
{
//friend class SolidworksModel;

public:
	SolidworksFile(string filePath);
	virtual ~SolidworksFile();

    bool save();

//private:
	void connect(IModelDoc* model) throw(DomeException); 
	void disconnect();

	string _filePath;
};
//***********************SolidworksFile********************


class SolidworksUnit : public SolidworksData, public DomeString
{
//friend class SolidworksModel;

public:
	virtual string getValue() throw(DomeException) = 0;
	virtual void setValue(string value) = 0;// throw(DomeException) = 0;
};

class SolidworksLengthUnit : public SolidworksUnit
{
//friend class SolidworksModel;

public:
//	SolidworksLengthUnit(){};
//	virtual ~SolidworksLengthUnit(){};

	string getValue() throw(DomeException);
	void setValue(string value);// throw(DomeException);
};


class SolidworksAngleUnit : public SolidworksUnit
{
//friend class SolidworksModel;

public:
	string getValue() throw(DomeException);
	void setValue(string value);// throw(DomeException);
};

class SolidworksMass : public DomeReal, public SolidworksData
{
//friend class SolidworksModel;

public:
	double getValue() throw(DomeException);
	void setValue(double value);// throw(DomeException);
};

class SolidworksVolume : public DomeReal, public SolidworksData
{
//friend class SolidworksModel;

public:
	double getValue() throw(DomeException);
	void setValue(double value);
};

class SolidworksSurfaceArea: public DomeReal, public SolidworksData
{
//friend class SolidworksModel;

public:
	double getValue() throw(DomeException);
	void setValue(double value);
};

class SolidworksColors: public /*DomeVector, */SolidworksData
{
//friend class SolidworksModel;

public:
	double getRed() throw(DomeException);
	double getGreen() throw(DomeException);
	double getBlue() throw(DomeException);
    vector <double> getValues() throw(DomeException);

protected:
    int getSize();// throw(DomeException);
    void setSize(int size);// throw(DomeException);

    void setValues(vector <double> values);// throw(DomeException);
    double getElement(int index) throw(DomeException);
    void setElement(int index) throw(DomeException);
};


} // namespace SolidworksPlugin
} // DOME

#endif // SOLIDWORKS_DATA_H
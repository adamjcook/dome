// SolidworksIncludes.h: interface for the SolidworksIncludes class.
//
//////////////////////////////////////////////////////////////////////

#ifndef SOLIDWORKS_INCLUDES_H
#define SOLIDWORKS_INCLUDES_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


// #define _WIN32_WINNT 0x0400 // required for multithreaded COM
//#include <string>

#include <afxpriv.h>	// only for the much needed T2COLE macro
#include <tchar.h>		// MultiByte & Unicode support

#include "swconst.h"
#include "swdisp.h"
// #include "swoptions.h"

#include <objbase.h>	// Multithreaded supported for COM

//#include <iostream>
//#include <vector>
//#include <string>
//using namespace std;

#include "DomePlugin.h"
using namespace DOME::DomePlugin;

#include "TypeConversions.h" // str
using namespace DOME::Utilities::TypeConversions;

#define SOLIDWORKS_ERROR1(msg) throw DomeException("Solidworks Error",msg)
#define SOLIDWORKS_ERROR2(msg) throw DomeException("Solidworks Error",__FILE__,__LINE__,msg)
#define SOLIDWORKS_ERROR SOLIDWORKS_ERROR2
//#define SOLIDWORKS_ERROR(msg) cout<<"Solidworks Error: "<<msg<<endl
#define SOLIDWORKS_DEBUG_SWITCH true
#define SOLIDWORKS_DEBUG(s) if (SOLIDWORKS_DEBUG_SWITCH) std::cerr<<"Solidworks Debug: "<<s<<std::endl;
//#define DOME_NOT_SUPPORTED(msg) 

#endif // SOLIDWORKS_INCLUDES_H

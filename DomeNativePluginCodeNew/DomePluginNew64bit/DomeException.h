// DomeException.h: interface for the DomeException class.
//
//////////////////////////////////////////////////////////////////////

#ifndef DOME_DOMEEXCEPTION_H
#define DOME_DOMEEXCEPTION_H

//#include "TypeConversions.h"
#include <exception>  // std::exception
#include <string>     // std::string
#include <iostream>   // std::cout, std::endl
#include <sstream>    // std::stringstream

/* CheckPoint is a handy way to print out where your code is. */

#ifdef _DEBUG
#define CheckPoint std::cout << __FILE__ << "(" << __LINE__ << ")" << std::endl
#else
#define CheckPoint
#endif

namespace DOME {
namespace DomePlugin {

/* 
The DomeException class should be wrapped in macros for each plugin as follows:
#define CUSTOM_ERROR(msg) throw DomeException("CustomError",msg)
#define CUSTOM_ERROR2(msg) throw DomeException("CustomError",__FILE__,__LINE__,msg)
*/
    class DomeException : public std::exception {
    protected:
      std::string message_;
    public:
      DomeException(const std::string& msg) throw();
      DomeException(const char* file, long line, const std::string& msg = "") throw();
      
      DomeException(const std::string& name, const std::string& msg) throw();
      DomeException(const std::string& name, const char* file, long line, const std::string& msg = "") throw();

      virtual ~DomeException() throw() { }
      virtual const char* what() const throw();

    private:
        std::string makeMessage(const std::string& name, const char* file, long line, const std::string& msg = "");
    };

    class DomeWarning : public DomeException {
    public:
        DomeWarning(const std::string& msg) throw() :
            DomeException("WARNING",msg) {};
    };

    class DomeNotSupported : public DomeException {
    public:
        DomeNotSupported(const std::string& msg) throw() :
            DomeException("NOT SUPPORTED",msg) {};
    };

    class DomeNotImplemented : public DomeException {
    public:
        DomeNotImplemented(const std::string& msg) throw() :
            DomeException("NOT IMPLEMENTED",msg) {};
    };

} // namespace DomePlugin
} // namespace DOME

#define DOME_WARNING(msg) throw DomeWarning(msg);
#define DOME_NOT_SUPPORTED(mthd) throw DomeNotSupported(mthd);
#define DOME_NOT_IMPLEMENTED(mthd) throw DomeNotImplemented(mthd);

#endif // DOME_DOMEEXCEPTION_H
